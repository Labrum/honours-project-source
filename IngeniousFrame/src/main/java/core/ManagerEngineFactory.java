package core;

import java.io.IOException;
import java.net.UnknownHostException;

import engines.DefaultEngine;
import engines.EMMEngine;
import engines.Engine;
import engines.EngineView;
import engines.FacilitatorEngine;
import engines.HumanEngine;
import engines.MCTSEngine;
import engines.MiniMaxEngine;
import engines.RandomEngine;

public class ManagerEngineFactory {

	private final String hostname;
	private final int port;

	public ManagerEngineFactory(String hostname, int port) {
		this.hostname = hostname;
		this.port = port;
	}

	public Engine generateEngine(String name, MatchSetting match, int position) {
		if (name.equals("Default Engine")) {
			System.out.println(name + " created at manager side");
			try {
				return new DefaultEngine(hostname, port, match, position);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (name.equals("Human Engine")) {
			try {
				return new EngineView(hostname, port, match, position);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (name.equals("MCTS Engine")) {
			try {
				return new MCTSEngine(hostname, port, match, position);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (name.equals("Minimax Engine")) {
			try {
				return new MiniMaxEngine(hostname, port, match, position);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (name.equals("EMM Engine")) {
			try {
				return new EMMEngine(hostname, port, match, position);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (name.equals("Random Engine")) {
			try {
				return new RandomEngine(hostname, port, match, position);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
}