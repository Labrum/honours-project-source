package core;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public final class Logging {

	private static Logger logger = null;
	
	public static void log(Level level, String message) {
		if (logger != null) {
			logger.log(level, message);
		}
	}
	
	public static void log(String message) {
		log(Level.INFO, message);
	}
	
	private static String printTime(long timeMillis){
		long yourmilliseconds = System.currentTimeMillis();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy-HH:mm");    
		Date resultdate = new Date(yourmilliseconds);
		return sdf.format(resultdate);
	}
	
	/**
	 * Sets logging to appear in a timestamped file in directory. Behavior is
	 * undefined if two instances of Orego are launched at the same millisecond.
	 */
	public static void setFilePath(String directory,String className) {
		logger = Logger.getLogger("Ingenious-default");
		new File(directory).mkdir();
		directory += File.separator + printTime(System.currentTimeMillis())+"-"+className + ".log";
		try {
			final FileHandler handler = new FileHandler(directory);
			handler.setFormatter(new Formatter() {
				@Override
				public String format(LogRecord record) {
					return record.getMessage() + "\n";
				}
			});
			logger.addHandler(handler);
			logger.setLevel(Level.ALL);
			logger.setUseParentHandlers(false);
		} catch (final Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
