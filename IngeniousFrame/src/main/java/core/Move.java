package core;

public class Move {

	private final Tile tile;
	private final Coordinate position;
	
	public Move(Tile tile, Coordinate position){
		this.tile = tile;
		this.position = position;
	}
	
	public Tile getTile(){
		return this.tile;
	}
	public Coordinate getPosition(){
		return this.position;
	}
	public String toString(){
		String ret = "Tile : "+tile.toString()+"\n";
		ret += "Coordinate : "+ position.toString();
		return ret;
	}
}