package core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import engines.Score;
import engines.ScoreKeeper;
import protocol.EngineConnection;

public class Controller implements Runnable {

	public final int NUMBER_OF_TILES;
	public final int NUMBER_OF_COLOURS;
	public final int NUMBER_OF_PLAYERS;

	private final ServerSocket serverSocket;
	private final int port;
	private final MatchSetting matchSettings;
	volatile int numberOfEngines = 0;
	private EngineConnection engineConns[];
	private volatile boolean gameLoop = true;
	private ArrayList<Rack> playerRacks;
	private BagSequence gameBag;
	private BoardInterface gameBoard;
	private ScoreKeeper scoreKeeper;

	/*
	 * Engines and variants are paired by key
	 */
	private String[] engineNames;
	private Variant[] variants;

	public Controller(MatchSetting match) throws Exception {
		this.matchSettings = match;
		this.engineNames = match.getEngineNames();
		this.NUMBER_OF_TILES = match.getBoardSize();
		this.NUMBER_OF_COLOURS = match.getNumColours();
		this.NUMBER_OF_PLAYERS = match.getNumPlayers();
		this.variants = match.getVariants();
		this.gameBoard = new Board(match.getBoardSize(),NUMBER_OF_COLOURS);
		this.scoreKeeper = new ScoreKeeper(NUMBER_OF_PLAYERS, NUMBER_OF_COLOURS);

		this.engineConns = new EngineConnection[matchSettings.getNumPlayers()];
		this.playerRacks = new ArrayList<Rack>(NUMBER_OF_PLAYERS);
		this.gameBag = new DefaultBag(120, NUMBER_OF_COLOURS);

		try {
			serverSocket = new ServerSocket(0);
			this.port = serverSocket.getLocalPort();
			System.out.println("Controller created on port" + this.port);

		} catch (IOException e) {
			throw new Exception("ServerSocket not created: Controller failed.");
		}
	}

	public int getPort() {
		return this.port;
	}
	
	public MatchSetting getMatchSetting(){
		return this.matchSettings;
	}

	public synchronized void addEngine(EngineConnection engineConn, int position) {
		if (engineConns[position] == null) {
			engineConns[position] = engineConn;
			numberOfEngines++;
			if (numberOfEngines == matchSettings.getNumPlayers()) {
				try {
					this.serverSocket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out
					.println("The number of engines has just been increaded to : "
							+ numberOfEngines);
		}
		for (int i = 0; i < engineConns.length; i++) {
			System.out.println(engineConns[i]);
		}
	}

	private void acceptJoiningPlayers() {
		while (numberOfEngines < matchSettings.getNumPlayers()) {
			try {
				Socket client = this.serverSocket.accept();

				EngineConnection engine = new EngineConnection(this, client);
			} catch (IOException e) {
				System.out.println("failed to connect new client");
			}
			System.out.println("At end of while loop: " + numberOfEngines);
		}
	}

	private void initGameRacks() {
		for (int i = 0; i < engineConns.length; i++) {
			playerRacks.add(i, getRack(variants[i].getRackSize()));
		}

		distributeRacks();
	}

	private Rack getRack(int rackSize) {
		if (rackSize == 0) {
			return null;
		}
		Rack rack = new Rack(rackSize);
		for (int i = 0; i < rackSize; i++) {
			rack.add(gameBag.draw());
		}

		return rack;
	}

	private void distributeRacks() {
		for (EngineConnection engine : engineConns) {
			for (int i = 0; i < this.NUMBER_OF_PLAYERS; i++) {
				if (variants[i].isOpponentsRackSizeVisible() || i == engine.id) {
					engine.setRack(playerRacks.get(i), i);
				}
			}
		}
	}

	private void distributeBags() {
		for (EngineConnection engine : engineConns) {
				engine.setBag(gameBag.viewVisible(gameBag.size()));
		}
	}

	private void distributeDraw(Tile drawnTile, int playerId) {
		for (EngineConnection engine : engineConns) {
			for (int i = 0; i < this.NUMBER_OF_PLAYERS; i++) {
				if (variants[i].isOpponentsRackSizeVisible() || i == playerId) {
					engine.draw(drawnTile, playerId);
				}
			}
		}
	}

	private void distributeAcceptedMove(int playerId, Move move) {
		for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
			if (i != playerId) {
				engineConns[i].playMove(move);
			}
		}
	}

	public void run() {
		acceptJoiningPlayers();

		System.out.println("CONTROLLER CONNECTION CONCLUDED");

		initGameRacks();
		distributeBags();

		System.out.println("CONTROLLER SETUP CONCLUDED");

		while (gameLoop) {
			for (EngineConnection engineTurn : engineConns) {
				int playerId = engineTurn.id;
				System.out.println("CURRENT PLAYER NUMBER : " + playerId);

				Move generatedMove = engineTurn.genMove();

				if (generatedMove == null) {
					System.out.println("generate move pass.");
					break;
				}

				if (this.gameBoard.makeMove(generatedMove)) {
					distributeAcceptedMove(playerId, generatedMove);
					System.out.println(gameBoard);
				} else {
					System.out.println(gameBoard.validMove(generatedMove));
					System.out.println("Invalid move");
					break;
				}

				System.out.println(generatedMove.getTile()
						+ " tile is removed from rack");

				playerRacks.get(playerId).remove(generatedMove.getTile());

				int[] result = scoreKeeper.updateScore(playerId, gameBoard);

				System.out.println(gameBoard);

				Score playerScore = scoreKeeper.getScore(playerId);

				if (playerScore.outrightWin(variants[playerId]
						.getMaximumColourScore())) {
					System.out.println("GAME WON BY PLAYER "
							+ engineNames[playerId]);
					for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
						System.out.println(scoreKeeper.getScore(i));
					}
					gameLoop = false;
					break;
				} else if (gameBoard.full()) {
					System.out.println("GAME WON BY PLAYER "
							+ scoreKeeper.getLeader());
					for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
						for (int j = 0; j < NUMBER_OF_COLOURS; j++) {
							System.out.print(scoreKeeper.getScore(i)
									.getColourScore(j) + " ");
						}
						System.out.println();
					}
					gameLoop = false;
					break;
				}
				Tile drawnTile = gameBag.draw();
				distributeDraw(drawnTile, playerId);
				playerRacks.get(playerId).add(drawnTile);

				System.out.println("Player Leading at end of round : "
						+ scoreKeeper.getLeader());
			}
		}
	}
}
