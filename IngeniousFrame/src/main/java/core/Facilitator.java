package core;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

import engines.Engine;
import protocol.ManagerConnection;
/**
 * 
 * Facilitator acts as server stores all started games 
 * and connection to managers. 
 * 
 * 
 * @author steven
 *
 */
public class Facilitator implements Runnable {

	private ServerSocket serverSocket = null;
	private Socket client = null;
	private int port;
	private volatile HashMap<String, Controller> startedGames = new HashMap<String, Controller>();
	private volatile HashMap<String, ManagerConnection> managers = new HashMap<String, ManagerConnection>();
	
	/**
	 * Create new Facilitator using the port specified
	 * @param port - specifies the port on which the facilitator will listen
	 * @throws Exception thrown if port is not available
	 */
	public Facilitator(int port) throws Exception {
		this.port = port;

		try {
			this.serverSocket = new ServerSocket(port);
			port = serverSocket.getLocalPort();
			Logging.setFilePath(Constants.LogDirectory,Facilitator.class.getName());
			Logging.log("Facilitator launched on port " + port);
			System.out.println("Facilitator launched on port " + port);
		} catch (IOException e) {
			throw new Exception("Port " + port
					+ " not available: Facilitator failed.");
		}
	}
	/**
	 * Look-up the port being used
	 * by a Controller based on the lobby-name
	 * 
	 * @param name
	 * @return int
	 */
	public int getControllerPort(String name){
		return startedGames.get(name).getPort();
	}
	
	public MatchSetting getMatchSettings(String name){
		return startedGames.get(name).getMatchSetting();
	}
	
	
	/**
	 * return the names of started controllers
	 * @return String[]
	 */
	public synchronized String[] getControllerNames(){
		
		String[] controllerNames = startedGames.keySet().toArray(new String[startedGames.size()]);
		return controllerNames;
	}
	
	/**
	 * Return a list of names for all managers currently connected
	 * to this facilitator
	 * @return String[]
	 */
	public synchronized String[] getPlayerNames(){
		String[] playerNames = managers.keySet().toArray(new String[managers.size()]);
		
		return playerNames;
	}
	
	/**
	 * Store a controller in the startedGames HashMap
	 * 
	 * @param lobbyName
	 * @param controller
	 */
	public synchronized void addLobby(String lobbyName, Controller controller) {

		startedGames.put(lobbyName, controller);
	}

	/**
	 * Store a connection to a manager in the managers HashMap
	 * 
	 * @param playerName
	 * @param manager
	 */
	public synchronized void addManager(String playerName,
			ManagerConnection manager) {
		managers.put(playerName, manager);
	}

	/**
	 * remove a previously stored manager
	 * @param playerName
	 */
	public synchronized void removeManager(String playerName) {
		managers.remove(playerName);
	}
	
	/**
	 * check the managers hashmap for a previous connection
	 * using the same name.
	 * @param playerName
	 * @return boolean
	 */
	public synchronized boolean checkDuplicateName(String playerName) {
		return managers.containsKey(playerName);
	}

	/**
	 * Listen for connecting managers.
	 */
	public void run() {
		while (true) {
			try {
				client = this.serverSocket.accept();

				Thread newStream = new Thread(new ManagerConnection(client,	this));
				newStream.start();				
				
			} catch (Exception e) {
				System.out.println("failed to connect new client");
				e.printStackTrace();
			}
		}
	}

	/**
	 * Launch the Facilitator
	 */
	public static void main(String[] args) {
		try {
			Facilitator face = new Facilitator(0);
			face.run();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}