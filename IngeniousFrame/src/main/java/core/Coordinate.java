package core;

public class Coordinate {
	private final int xPosition;
	private final int yPosition;
	
	public Coordinate(int x, int y){
		this.xPosition = x;
		this.yPosition = y;
	}
	
	public Coordinate(double d, double e) {
		xPosition = (int) Math.round(d);
		yPosition = (int) Math.round(e);
	}
	
	public static Coordinate hexRound(double q, double r){
		double x =q;
		double z =r;
		double y = -x-z;
		
		int rx = (int) Math.round(x);
		int rz = (int)  Math.round(z);
		int ry = (int) Math.round(y);
		
	    double x_diff = Math.abs(rx - x);
	    double y_diff = Math.abs(ry - y);
	    double z_diff = Math.abs(rz - z);
	    
	    if (x_diff > y_diff && x_diff > z_diff){
	        rx = -ry-rz ;
	    }else if( y_diff > z_diff)
	        ry = -rx-rz;
	    else{
	        rz = -rx-ry;
	    }
	    
		return new Coordinate(rx,rz);
	}

	public int getX(){
		return xPosition;
	}
	
	public int getY(){
		return yPosition;
	}
	
	public Coordinate add(Coordinate coord){
		return new Coordinate(coord.xPosition+this.xPosition,coord.yPosition+this.yPosition);
	}
	
	public Coordinate sub(Coordinate coord){
		return new Coordinate(this.xPosition-coord.xPosition,this.yPosition-coord.yPosition);
	}
	
	public Coordinate negate(){
		return new Coordinate(-xPosition,-yPosition);
	}
	
	public boolean equals(Object o){
		
		if (!(o instanceof Coordinate)){
			return false;
		}
		
		Coordinate oCoord = (Coordinate) o;
		
		if(oCoord.getX() == this.getX()){
			if(oCoord.getY()==this.getY()){
				return true;
			}
		}
		
		return false;
	}
	
	public int hashCode(){
		int hash = 7;
	    hash = 71 * hash + this.xPosition;
	    hash = 71 * hash + this.yPosition;
	    return hash;
	}
	
	public String toString(){
		return "("+this.xPosition+" , "+ this.yPosition+")";
	}
}
