package core;

/**
 * Tile object represents a game tile.
 * The rotation is based on the bottom hex being the pivot.
 * The two ints represent the colour of the individual hexes.
 * 
 * @author steven
 */


public class Tile {
	private final int Top;
	private final int Bottom;
	private int rotation; // an int between 0 - 5 that which in a antiClockwise fashion indicates the roation of the tile.
	private static final Coordinate TOP_LEFT =new Coordinate(0,-1);
	private static final Coordinate LEFT =new Coordinate(-1,0);
	private static final Coordinate BOTTOM_LEFT =new Coordinate(-1,1);
	private static final Coordinate BOTTOM_RIGHT =new Coordinate(0,1);
	private static final Coordinate RIGHT =new Coordinate(1,0);
	private static final Coordinate TOP_RIGHT =new Coordinate(1,-1);
				
	public static final Coordinate [] coordinateSystem = {TOP_LEFT,LEFT,BOTTOM_LEFT,BOTTOM_RIGHT,RIGHT,TOP_RIGHT};
		
	
	public Tile(int first, int second){
		this.Top = first;
		this.Bottom = second;
		this.rotation = 0;
	}

	public int getTopColour() {
		return Top;
	}
	
	/**
	 * Copy returns a tile with the same values.
	 * The rotation is not set.
	 * @return Tile
	 */
	public Tile copy(){
		return new Tile(Top,Bottom);
	}
	
	public boolean equals(Object o){
		if (o instanceof Tile){
			Tile tile = (Tile) o;
			if((tile.Top == this.Top) &&(tile.Bottom == this.Bottom) ){
				return true;
			} else if((tile.Top == this.Bottom ) &&(tile.Bottom == this.Top )){
				return true;
			}
		}
		return false;
	}

	public Coordinate getTopCoord(Coordinate bottomPosition){
		return bottomPosition.add(coordinateSystem[this.rotation]);
	}
	
	public int getBottomColour() {
		return Bottom;
	}
	
	public int getRotation() {
		return rotation;
	}
	
	public void setRotation(int rotation, int NumberOfColours){
		this.rotation = rotation%NumberOfColours;
	}
	
	public void incrementRotation( int NumberOfColours){
		this.rotation = this.rotation++;
		this.rotation = this.rotation%NumberOfColours;
	}

	public String toString(){
		return "("+this.Top+"-"+this.Bottom+") rotation => "+this.rotation;
	}		
}