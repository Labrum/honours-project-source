package core;

import java.util.ArrayList;

import engines.ScoreKeeper;

public class GameState {

	private final Board gameBoard;
	private final int currentPlayer;
	private final ScoreKeeper currScore;
	private final ArrayList<ArrayList<Tile>> racks;
	private final BagSequence currentBag;
	
	public GameState(Board currBoard, int nextPlayer,ScoreKeeper scorekeeper,ArrayList<ArrayList<Tile>> racks, BagSequence bag ){
		 gameBoard= currBoard;
		 currentPlayer = nextPlayer;
		 currScore = scorekeeper;
		 currentBag = bag;
		 this.racks = racks;
	}

	public Board getGameBoard() {
		return gameBoard;
	}

	public int getCurrentPlayer() {
		return currentPlayer;
	}

	public ScoreKeeper getCurrScore() {
		return currScore;
	}

	public BagSequence getCurrentBag() {
		return currentBag;
	}
	
}
