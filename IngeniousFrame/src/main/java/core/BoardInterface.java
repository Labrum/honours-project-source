package core;

import java.util.ArrayList;
/**
 * BoardInterface
 * 
 * @author steven
 *
 */
public interface BoardInterface {
	
	public boolean full();
	public boolean validMove(Move move);
	public boolean makeMove(Move move);
	public Move lastMove();
	public int getHex(Coordinate position);
	public ArrayList<Move> generateMoves(Rack rack);
	public void undoMove();
	public int getNumColours();
}