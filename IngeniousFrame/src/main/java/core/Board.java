package core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.Stack;
/**
 * Hexagonal board for Ingenious
 * 
 * @author steven
 *
 */
public class Board implements BoardInterface {

	private HashMap<Coordinate, Integer> gameBoard;
	public final int DIAMETER; // Diameter must be a odd number greater than or
								// equal to 3
	public final int SIDE_LENGTH;
	public final int CAPACITY;
	private Move LAST_MOVE = null;
	private final int NUMBER_COLOURS;
	private Stack<Move> moveHistory = new Stack<Move>();

	/**
	 * @return int - number of colours
	 */
	public int getNumColours(){
		return NUMBER_COLOURS;
	}
	
	public Board copy(){
		Board copy = new Board(DIAMETER,NUMBER_COLOURS);
		for(Move m : this.moveHistory){
			copy.makeMove(m);
		}
		return copy;
	}
	
	public Stack<Move> getMoveHistory(){
		return moveHistory;
	}
	
	/**
	 * Create a new Board
	 * 
	 * @throws IllegalArgumentException
	 * @param diameter
	 * @param NUMBER_COLOURS
	 */
	public Board(int diameter, int NUMBER_COLOURS) {
		this.NUMBER_COLOURS = NUMBER_COLOURS;
		this.DIAMETER = diameter;
		
		if(this.DIAMETER<3 || this.DIAMETER%2==0){
			throw new IllegalArgumentException("Board Diameter must be odd and greater than 3.");
		}
		
		this.SIDE_LENGTH = diameter / 2 + 1;

		this.CAPACITY = capacity(DIAMETER);
		
		gameBoard = new HashMap<Coordinate, Integer>();

		Coordinate c = new Coordinate(0, -SIDE_LENGTH + 1);
		gameBoard.put(c, 0);

		c = new Coordinate(-SIDE_LENGTH + 1, 0);
		gameBoard.put(c, 1);

		c = new Coordinate(SIDE_LENGTH - 1, -SIDE_LENGTH + 1);
		gameBoard.put(c, 5);

		c = new Coordinate(SIDE_LENGTH - 1, 0);
		gameBoard.put(c, 4);

		c = new Coordinate(-SIDE_LENGTH + 1, SIDE_LENGTH - 1);
		gameBoard.put(c, 2);

		c = new Coordinate(0, SIDE_LENGTH - 1);
		gameBoard.put(c, 3);

	}
	/**
	 * Calculates the capacity of a board with size DIAMETER
	 * 
	 * @param DIAMETER
	 * @return int capacity
	 */
	public static int capacity(int DIAMETER){
		int capacity = 0;
		int side_length = DIAMETER/2;
		
		capacity = DIAMETER*(side_length);
		
		capacity = capacity - (side_length*((side_length)+1))/2;
		
		capacity = capacity*2+DIAMETER;
		
		return capacity;
	}

	/**
	 * Generate a list of available moves based on a rack.
	 */
	public ArrayList<Move> generateMoves(Rack rack) {
		ArrayList<Move> moves = new ArrayList<Move>();
		Move nextMove;
		Coordinate pos;
		for (Tile tile : rack) {
			for (int i = -SIDE_LENGTH + 1; i < SIDE_LENGTH; i++) {
				for (int j = -SIDE_LENGTH + 1; j < SIDE_LENGTH; j++) {
					pos = new Coordinate(i, j);
					for (int k = 0; k < Tile.coordinateSystem.length; k++) {
						Tile temp = new Tile(tile.getTopColour(),
								tile.getBottomColour());
						temp.setRotation(k,NUMBER_COLOURS);
						nextMove = new Move(temp, pos);
						if (validMove(nextMove)) {
							moves.add(nextMove);
						}
					}
				}
			}
		}
		//System.out.println("LENGTH OF LIST SENT FROM BOARD :"+moves.size());
		return moves;
	}
	
	/**
	 * if no more tiles can be placed the board is full
	 * 
	 * @return boolean
	 */
	public boolean full() {
		for (int i = -SIDE_LENGTH + 1; i < SIDE_LENGTH; i++) {
			for (int j = -SIDE_LENGTH + 1; j < SIDE_LENGTH; j++) {
				if (getHex(i, j) == -1) {
					Coordinate coord = new Coordinate(i, j);
					for (Coordinate direction : Tile.coordinateSystem) {
						Coordinate test = coord;
						test = test.add(direction);
						if (test.getX() < SIDE_LENGTH
								&& test.getX() > -SIDE_LENGTH) {
							if (test.getY() < SIDE_LENGTH
									&& test.getY() > -SIDE_LENGTH) {
								if (getHex(test) == -1) {
									return false;
								}
							}
						}
					}
				}
			}
		}
		return true;
	}

	/**
	 * Get the int value of the hex at (q,r)
	 * 
	 * @param q
	 * @param r
	 * @return int
	 */
	public int getHex(int q, int r) {
		Coordinate coord = new Coordinate(q, r);
		return getHex(coord);
	}

	/**
	 * Validity check for move
	 * 
	 * @return boolean
	 */
	public boolean validMove(Move move) {
		Coordinate botPos = move.getPosition();
		Coordinate topPos = move.getTile().getTopCoord(move.getPosition());

		if ((botPos.getX() > this.DIAMETER / 2)
				|| (botPos.getX() < -this.DIAMETER / 2)) {
			return false;
		} else if ((botPos.getY() > this.DIAMETER / 2)
				|| (botPos.getY() < -this.DIAMETER / 2)) {
			return false;
		}

		if ((topPos.getX() > this.DIAMETER / 2)
				|| (topPos.getX() < -this.DIAMETER / 2)) {
			return false;
		} else if ((topPos.getY() > this.DIAMETER / 2)
				|| (topPos.getY() < -this.DIAMETER / 2)) {
			return false;
		}

		if (this.getHex(botPos) == -1) {
			if (this.getHex(topPos) == -1) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Get the set of all occupied hex's
	 * 
	 * @return Set<Coordinate>
	 */
	public Set<Coordinate> getCoordinates() {
		return gameBoard.keySet();
	}

	/**
	 * Return string representation of the board
	 * @return String
	 */
	public String toString() {
		String result = "";
		int prefixLength = SIDE_LENGTH - 1;
		int count = 0;
		boolean reachedMiddleLine = false;
		int k = 0;
		for (int q = -(SIDE_LENGTH - 1); q < (SIDE_LENGTH); q++) {

			for (int i = 0; i < prefixLength; i++) {
				result += " ";
			}
			if (prefixLength == 0) {
				reachedMiddleLine = true;
			}
			if (!reachedMiddleLine) {
				prefixLength--;

			} else {
				prefixLength++;
			}

			if (!reachedMiddleLine) {

				for (int r = k; r < SIDE_LENGTH; r++) {
					result += getHex(r, q);
					result += "|";
				}
				k--;
			} else {
				for (int r = -(SIDE_LENGTH - 1); r < SIDE_LENGTH - prefixLength
						+ 1; r++) {
					result += getHex(r, q);
					result += "|";
				}
			}
			count++;
			result += "\n";
		}
		return result;
	}

	/**
	 * return the last move that was made
	 * 
	 * @return Move
	 */
	public Move lastMove() {
		return LAST_MOVE;
	}

	/**
	 * undo the last move made
	 */
	public void undoMove() {
		gameBoard.remove(LAST_MOVE.getPosition());
		gameBoard.remove(LAST_MOVE.getTile().getTopCoord(
		LAST_MOVE.getPosition()));
		moveHistory.pop();
		if (moveHistory.size() > 0) {
			LAST_MOVE = moveHistory.peek();
		}
	}

	/**
	 * Get the int value of the hex at Coordinate
	 * 
	 * @return int
	 */
	public int getHex(Coordinate position) {
		if (gameBoard.containsKey(position)) {

			return gameBoard.get(position);
		}
		return -1;
	}

	/**
	 * make move on gameboard
	 */
	public boolean makeMove(Move move) {
		if (!validMove(move)) {
			return false;
		}

		Integer bottomColour = gameBoard.put(move.getPosition(), move.getTile()
				.getBottomColour());
		Integer topColour = gameBoard.put(
				move.getTile().getTopCoord(move.getPosition()), move.getTile()
						.getTopColour());

		if ((bottomColour == null) && (topColour == null)) {
			LAST_MOVE = move;
			moveHistory.push(LAST_MOVE);
			return true;
		}

		gameBoard.remove(move.getPosition());
		gameBoard.remove(move.getTile().getTopCoord(move.getPosition()));
		return false;
	}

	public static void main(String[] args) {
		System.out.println(Board.capacity(3));
		Board board = new Board(7,6);
		Tile tile = new Tile(4, 4);
		tile.setRotation(4, 6);
		Coordinate coord = new Coordinate(0, 0);
		Move move = new Move(tile, coord);
		System.out.println(board.validMove(move));
		System.out.println(board.makeMove(move));
		System.out.println(board);
		System.out.println(board.getHex(-1, -1));
		System.out.println(board.lastMove());
		System.out.println(board.full());

	}
}
