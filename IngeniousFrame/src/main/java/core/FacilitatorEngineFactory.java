package core;

import java.io.IOException;
import java.net.UnknownHostException;

import engines.EMMEngine;
import engines.Engine;
import engines.FacilitatorEngine;
import engines.MCTSEngine;
import engines.MiniMaxEngine;
import engines.RandomEngine;

public class FacilitatorEngineFactory {
	private final String hostname;
	private final int port;	
	public FacilitatorEngineFactory(String hostname, int port){
		this.hostname = hostname;
		this.port = port;
	}
	
	public Engine generateEngine(String name,MatchSetting match, int position){
		if(name.equals("Facilitator Engine")){

			try {
				return new FacilitatorEngine(hostname,port, match,position);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}else if(name.equals("Random Engine")){
			try {
				return new RandomEngine(hostname,port, match,position);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(name.equals("Minimax Engine")){
			try {
				return new MiniMaxEngine(hostname,port, match,position);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(name.equals("EMM Engine")){
			try {
				return new EMMEngine(hostname,port, match,position);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(name.equals("MCTS Engine")){
			try {
				return new MCTSEngine(hostname,port, match,position);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
}
