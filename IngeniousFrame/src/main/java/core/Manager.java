package core;

import engines.Engine;
import gui.Panel;
import gui.WelcomePanel;

import java.awt.BorderLayout;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.TreeMap;

import javax.swing.JFrame;

import logging.JSON;


public class Manager {

	ObjectInputStream inputStream;
	ObjectOutputStream outputStream;
	String host = null;
	int port = -1;
	private JFrame frame;
	private Engine engine;
	private Panel panel;
	private Socket socket;
	private String playerName;
	private final String VARIANTS_PATH = "src/main/resources/Variants/Manager";
	private final String SETTINGS_PATH = "src/main/resources/MatchSettings/Manager";
	private static final String[] ManagerPlayers = {"Human Engine"};
	private final String[] ManagerVariants;
	private final String[] ManagerSettings;
	
	private String[] setManagerVariants(String path){
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
		String [] variantNames = new String [listOfFiles.length];
	    for (int i = 0; i < listOfFiles.length; i++) {
	      if (listOfFiles[i].isFile()) {
	    	 variantNames[i] = listOfFiles[i].getName();
	      } 
	    }
		return variantNames;
	}
	
	private String[] setManagerSettings(String path){
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
		String [] settingsNames = new String [listOfFiles.length];
	    for (int i = 0; i < listOfFiles.length; i++) {
	      if (listOfFiles[i].isFile()) {
	    	 settingsNames[i] = listOfFiles[i].getName();
	      } 
	    }
		return settingsNames;
	}
	
	public void minimizeFrame(){
		this.frame.setState(JFrame.ICONIFIED);
	}
	
	public int controllerPort(String name) throws IOException, ClassNotFoundException{
		
		outputStream.writeObject("ControllerPort");
		outputStream.writeObject(name);
		int port = (Integer) inputStream.readObject();
		return port;
	}
	
	public MatchSetting requestMatchSetting(String name) throws IOException, ClassNotFoundException{
		
		outputStream.writeObject("MatchSetting");
		outputStream.writeObject(name);
		MatchSetting match = (MatchSetting) inputStream.readObject();
		
		return match;
	}
	
	
	public boolean launchEngines(TreeMap<Integer, String> managerEngines,MatchSetting match){
		int controllerPort = -1;
		try {
			controllerPort = (Integer)inputStream.readObject();
			System.out.println("Received port");
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		ManagerEngineFactory factory = new ManagerEngineFactory(this.host, controllerPort);
		for(int i : managerEngines.keySet()){
			System.out.println(managerEngines.get(i));
			Thread thread = new Thread(factory.generateEngine(managerEngines.get(i),match, i));
			thread.start();
		}

		return true;
	}
	
	/**
	 * Receives the list of engines chosen by a user and returns a
	 * Treemap that contains all the manager only engines. Treemap
	 * key is the position in the array.
	 * 
	 * 
	 * @param engineNames
	 * @return managerOnlyEngines
	 */
	public TreeMap<Integer,String> identifyManagerEngines(String[] engineNames){
		
		TreeMap<Integer,String> managerOnlyEngines = new TreeMap<Integer,String>();
		
		for(int i =0; i< engineNames.length;i++){
			for(String ManagerEngine : ManagerPlayers){
				if(engineNames[i].equals(ManagerEngine)){
					managerOnlyEngines.put(i, engineNames[i]);
					break;
				}
			}
		}

		return managerOnlyEngines;
	}

	private void setupFrame() {
		frame = new JFrame();
		frame.setBounds(100, 100, 480, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.setVisible(true);
	}
	
	public void pack(){
		frame.pack();
	}

	public String[] refreshLobby() {
		try {
			outputStream.writeObject(new String("Refresh"));
			System.out.println("Refresh requested");
			String[] controllers = (String[]) inputStream.readObject();
			return controllers;
		} catch (IOException e) {
			return null;
		} catch (ClassNotFoundException e) {
			return null;
		}
	}

	public boolean newMatch(MatchSetting match) {
		try {
			if (match == null) {
				System.out.println("match is null");
			}
			outputStream.writeObject(new String("NewMatch"));

			outputStream.writeObject(match);
			System.out.println("New Match requested");
			return true;
		} catch (IOException e) {
			return false;
		}

	}

	public void setPlayerName(String name) {
		playerName = name;
	}

	public String connect(String hostName, int port, String playerName) {
		try {
			socket = new Socket(hostName, port);
			openStreams();
			writeObject(new String(playerName));

			String ack = (String) inputStream.readObject();

			if (ack.equals("Acknowledged")) {
				setPlayerName(playerName);
				return "Connected";
			} else {
				closeConnection();
				return "Player name taken";
			}
		} catch (UnknownHostException e) {
			return "Hostname or port incorrect";

		} catch (IOException e) {
			return "IO EXCEPTION";

		} catch (ClassNotFoundException e) {
			return "Unexpected object received from Facilitator";

		}

	}

	public void writeObject(Object obj) throws IOException {
		outputStream.writeObject(obj);
	}

	public Manager() {

		this.ManagerVariants = setManagerVariants(VARIANTS_PATH);
		this.ManagerSettings = setManagerSettings(SETTINGS_PATH);
		setupFrame();
		panel = new WelcomePanel(this);
		swapPanel(panel);
		panel.addLogic();
	}

	public static void main(String[] args) {

		Logging.setFilePath(Constants.LogDirectory,Manager.class.getName());
		Logging.log("Manager started");
		Manager mng = new Manager();
	}

	public void swapPanel(Panel panel) {

		frame.getContentPane().removeAll();

		frame.getContentPane().add(panel.getPanel());
		frame.pack();
		frame.revalidate();
		frame.repaint();

	}

	/**
	 * Opens IO-Streams catches and prints failures
	 */
	public void openStreams() {
		try {
			outputStream = new ObjectOutputStream(this.socket.getOutputStream());

		} catch (IOException e) {
			System.out.println("Output Stream failed to open.");
		}
		try {
			inputStream = new ObjectInputStream(this.socket.getInputStream());
		} catch (IOException e) {
			System.out.println("Input Stream failed to open.");
		}
	}

	/**
	 * Disconnects IO-streams and closes socket catches and prints failures
	 */
	public void closeConnection() {
		try {
			inputStream.close();
		} catch (IOException e) {
			System.out.println("Input stream failed to close");
		}

		try {
			outputStream.close();
		} catch (IOException e) {
			System.out.println("Input stream failed to close");
		}

		try {
			socket.close();
		} catch (IOException e) {
			System.out.println("Socket failed to close");
		}
	}

	public String[] getManagerPlayers() {
		return ManagerPlayers;
	}

	public String[] getManagerVariants() {
		return ManagerVariants;
	}
	public String[] getManagerSettings() {
		return ManagerSettings;
	}
	
	public MatchSetting unpackMatchSetting(String name){
		MatchSetting match=null;
		FileInputStream fin;
		try {
			JSON json = new JSON("");
			match = json.readMatchSettings(SETTINGS_PATH+"/"+name);
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
		
		return match;
	}
	
	public Variant[] unpackVariants(String[] variants) {
		
		Variant[] vars = new Variant[variants.length];
		
		for(int i =0; i< variants.length;i++){
			FileInputStream fin;
			try {
				JSON json = new JSON("");
				vars[i] = json.readVariantSettings(VARIANTS_PATH+"/"+variants[i]);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
		return vars;
	}
}