package core;

import java.io.Serializable;
import java.util.ArrayList;

public class MatchSetting implements Serializable {
	/**
	 * Determines if a de-serialized file is compatible with this class.
	 */
	private static final long serialVersionUID = -3834774530442103753L;
	private final String LobbyName;
	private final int numPlayers;

	private final int [] colourRatio;
	private final int numColours;
	private final int boardSize;
	private final Variant[] variants;
	private final String [] engineNames;
	private final ArrayList<Integer> openSlots;
	private final boolean managerPlaying;
	
	public static class MatchBuilder{
		private final int numPlayers;
		private final String LobbyName;
		private final int numColours;
		private int boardSize;
		private int [] colourRatio;
		private Variant[] variants;
		private String [] engineNames;
		private ArrayList<Integer> openSlots;
		private boolean managerPlaying = false;
		
		public MatchBuilder(String name,int numPlayers, int numColours){
			this.LobbyName = name;
			this.numPlayers = numPlayers ;
			this.numColours = numColours;
		}
		
		/**
		 * updates the value of emptySlots
		 */
		private void updateEmptySlots(){
			openSlots = new ArrayList<Integer>();
			
			for(int i =0; i < engineNames.length;i++){
				if(engineNames[i].equals("Open Slot")){
					openSlots.add(i);
				}
			}
		}
		
		public void setEngines(String[] engines){
			engineNames = engines;
			updateEmptySlots();
						
		}
		
		public void setEngine(String engine, int index){
			engineNames[index] = engine;
			updateEmptySlots();
		}

		public void setPlaying(boolean playing){
			managerPlaying = playing;
		}
		
		public void setBoardSize(int boardSize){
			this.boardSize = boardSize;
		}
		
		public void setVariants(Variant[] vars){
			variants = vars;
		}
		
		public void setVariant(Variant var, int index){
			variants[index] = var;
		}
		
		public void setColourRatio(int [] colourRatio){
			this.colourRatio = colourRatio;
		}
		
		public MatchSetting buildMatch(){
			
			MatchSetting match = new MatchSetting(this);
			return match;
		}
	}
	private MatchSetting(MatchBuilder matchBuilder){
		LobbyName = matchBuilder.LobbyName;
		numPlayers = matchBuilder.numPlayers;
		numColours = matchBuilder.numColours;
		colourRatio = matchBuilder.colourRatio;
		boardSize = matchBuilder.boardSize;
		variants = matchBuilder.variants;
		engineNames = matchBuilder.engineNames;
		openSlots = matchBuilder.openSlots;
		managerPlaying = matchBuilder.managerPlaying;
	}
	
	public String getName(){
		return LobbyName;
	}
	public int getNumPlayers(){
		return numPlayers;
	}
	public int getNumColours(){
		return numColours;
	}
	public int[] getColourRatio(){
		return colourRatio;
	}
	public Variant[] getVariants(){
		return variants;
	}
	public int getBoardSize(){
		return boardSize;
	}
	public String[] getEngineNames(){
		return engineNames;
	}
	public ArrayList<Integer> getOpenSlots(){
		return openSlots;
	}
	public boolean createrJoining(){
		return managerPlaying;
	}
}