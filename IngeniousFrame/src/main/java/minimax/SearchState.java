package minimax;

import java.util.ArrayList;

import core.BoardInterface;
import core.Move;
import core.Rack;
import engines.ScoreKeeper;

abstract public class SearchState {
	
	private BoardInterface board;
	private ScoreKeeper scores;
	private ArrayList<Rack> racks;
	private int currentPlayer = 0;
	
	public SearchState(BoardInterface board, ScoreKeeper scores,  ArrayList<Rack> racks, int player){
		this.setBoard(board);
		this.setScores(scores.copy());
		this.setRacks(new ArrayList<Rack>());
		this.setCurrentPlayer(player);
		
		for(Rack r : racks){
			this.getRacks().add(r.copy());
		}
	}
	
	public ArrayList<Move> generateMoves(){
		
		return getBoard().generateMoves(getRacks().get(getCurrentPlayer()));
		
	}

	public BoardInterface getBoard() {
		return board;
	}

	public void setBoard(BoardInterface board) {
		this.board = board;
	}

	public int getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}

	public ArrayList<Rack> getRacks() {
		return racks;
	}

	public void setRacks(ArrayList<Rack> racks) {
		this.racks = racks;
	}

	public ScoreKeeper getScores() {
		return scores;
	}

	public void setScores(ScoreKeeper scores) {
		this.scores = scores;
	}	
	
}
