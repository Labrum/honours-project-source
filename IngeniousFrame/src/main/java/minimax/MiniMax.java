package minimax;

import java.util.ArrayList;

import core.Move;
import core.Tile;
import engines.Engine;
import engines.Evaluator;
import engines.FullInformationState;
import engines.IMoveController;

/**
 * A abstract minimax implementation can be extended for different minimax
 * enhancements. Extensions should be as domain independant as possible.
 * 
 * @author steven
 *
 */
abstract public class MiniMax implements IMoveController {

	public Evaluator evaluator;
	protected int playerId;

	/*
	 * opponentId is only applicable to 2 player minimax.
	 */
	protected int opponentId;

	protected int numberOfColours;
	public int searchDepth;

	/**
	 * getState should return a state that can be used by the minimax algorithm
	 * implemented. Engine components should be copied rather than referenced.
	 * 
	 * getState is called at the start of the search and updated throughout the
	 * search.
	 * 
	 * @return
	 */
	public SearchState getState(Engine engine) {

		FullInformationState state = new FullInformationState(engine.gameBoard,
				engine.scoreKeeper.copy(), engine.racks, engine.expectedBag,
				engine.playerId);

		return state;
	}

	public void setLocalVariables(Engine engine) {

		this.playerId = engine.playerId;
		this.opponentId = 1 - this.playerId;
		this.numberOfColours = engine.matchSetting.getNumColours();
	}

	public Move generateMove(Engine engine, Evaluator evaluator) {

		this.evaluator = evaluator;

		/*
		 * The gameState contains all information that changes at different
		 * ply's.
		 */
		SearchState gameState = getState(engine);

		/*
		 * set the localVariables which are used throughout the search without
		 * being changed.
		 */
		this.evaluator = evaluator;
		setLocalVariables(engine);



		ArrayList<Move> moves = gameState.generateMoves();

		Move bestMove = moves.get(0);
		double bestValue = Double.MIN_VALUE+1;
		double alpha = Double.MIN_VALUE+1;
		double beta = Double.MAX_VALUE;
		
		for (Move child : moveOrder(moves,gameState)) {
			
			this.preSearchStateAdjustment(gameState);
			makeMove(child, gameState);
			double value = MinMove(gameState,alpha,beta, 0);
			unmakeMove(child, gameState);

			this.postSearchStateAdjustment(gameState);

			if (bestValue < value) {
				bestValue = value;
				bestMove = child;
			}
			
			alpha = Math.max(alpha, bestValue);
			if(bestValue >=beta){
				break;
			}

		}

		return bestMove;
	}

	/**
	 * Make move includes all affects to the state of the game not just the
	 * placement of a tile on the board
	 */
	public void makeMove(Move child, SearchState gameState) {

		try {
			FullInformationState state = (FullInformationState) gameState;

			state.getBoard().makeMove(child);
			state.getRacks().get(state.getCurrentPlayer()).remove(child.getTile());

			Tile bagTile = state.getSearchBag().get(state.getBagIndex());
			state.getRacks().get(state.getCurrentPlayer()).add(bagTile);

			state.getScores().updateScore(playerId, state.getBoard());
			state.setBagIndex(state.getBagIndex() + 1);
			state.setCurrentPlayer(1 - state.getCurrentPlayer());
		} catch (ClassCastException e) {
			throw new RuntimeException(
					"SearchState not compatible with minimax enhancements");
		}

	}

	public void unmakeMove(Move child, SearchState gameState) {
		try {
			FullInformationState state = (FullInformationState) gameState;

			state.setCurrentPlayer(1 - state.getCurrentPlayer());
			state.setBagIndex(state.getBagIndex() - 1);

			state.getBoard().undoMove();
			state.getRacks().get(state.getCurrentPlayer()).add(child.getTile());

			Tile bagTile = state.getSearchBag().get(state.getBagIndex());
			state.getRacks().get(state.getCurrentPlayer()).remove(bagTile);

			state.getScores().undoScore(state.getCurrentPlayer(), state.getBoard());
		} catch (ClassCastException e) {
			throw new RuntimeException(
					"SearchState not compatible with minimax enhancements");
		}
	}

	
	public void postSearchStateAdjustment(SearchState state) {

	}

	public void preSearchStateAdjustment(SearchState state) {

	}

	public ArrayList<Move> moveOrder(ArrayList<Move> moves,SearchState state) {
		/*
		 * default move ordering is random
		 */
		return moves;
	}

	public double MaxMove(SearchState state,double alpha,double beta, int depth) {

		if (state.getBoard().full() || this.searchDepth == depth) {
			return evaluator.evaluate(state.getScores(), state.getBoard(), state.getRacks(),this.playerId);
		}

		double bestValue =  Double.MIN_VALUE+1;

		ArrayList<Move> moves = state.generateMoves();
		for (Move child : moveOrder(moves,state)) {
			this.preSearchStateAdjustment(state);

			makeMove(child, state);
			double value = MinMove(state,alpha,beta, depth +1);
			unmakeMove(child, state);

			this.postSearchStateAdjustment(state);

			if (bestValue < value) {
				bestValue = value;
			}
			
			alpha = Math.max(alpha, bestValue);
			if(bestValue >=beta){
				break;
			}
		}

		return bestValue;
	}

	public double MinMove(SearchState state,double alpha,double beta, int depth) {

		if (state.getBoard().full() || this.searchDepth == depth) {
			return evaluator.evaluate(state.getScores(), state.getBoard(), state.getRacks(),this.playerId);
		}

		double bestValue =  Double.MAX_VALUE;

		ArrayList<Move> moves = state.generateMoves();

		for (Move child : moveOrder(moves,state)) {
			this.preSearchStateAdjustment(state);

			makeMove(child, state);
			double value = MaxMove(state,alpha,beta, depth + 1);
			unmakeMove(child, state);

			this.postSearchStateAdjustment(state);

			if (bestValue > value) {
				bestValue = value;
			}
			
			beta = Math.min(beta, bestValue);
			if(bestValue <=alpha){
				break;
			}

		}

		return bestValue;
	}
}
