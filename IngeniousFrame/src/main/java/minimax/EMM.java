package minimax;

import java.util.ArrayList;

import core.Board;
import core.Move;
import core.Tile;
import engines.Engine;
import engines.Evaluator;
import engines.FullInformationState;


public class EMM extends MiniMax {

	public EMM(int depth){
		this.searchDepth = depth;
		
	}
	
	private final static ArrayList<Tile> possibleTiles = new ArrayList<Tile>();
	
	private int calcPossibleTiles(int numberOfColours){
		return (numberOfColours*(numberOfColours+1))/2;
	}
	private static Tile getPossibleTile(int numberInPossibleTiles){
		
		return possibleTiles.get(numberInPossibleTiles);
		
	}
	
	public Move generateMove(Engine engine, Evaluator evaluator) {

		this.evaluator = evaluator;

		/*
		 * The gameState contains all information that changes at different
		 * ply's.
		 */
		SearchState gameState = getState(engine);

		/*
		 * set the localVariables which are used throughout the search without
		 * being changed.
		 */
		this.evaluator = evaluator;
		setLocalVariables(engine);



		ArrayList<Move> moves = gameState.generateMoves();

		Move bestMove = moves.get(0);
		double bestValue = Double.MIN_VALUE+1;
		double alpha = Double.MIN_VALUE+1;
		double beta = Double.MAX_VALUE;
		
		for (Move child : moveOrder(moves,gameState)) {
			
			this.preSearchStateAdjustment(gameState);
			makeMove(child, gameState);
			double value = chanceNode(gameState,0,alpha,beta, true);
			unmakeMove(child, gameState);

			this.postSearchStateAdjustment(gameState);

			if (bestValue < value) {
				bestValue = value;
				bestMove = child;
			}
			
			alpha = Math.max(alpha, bestValue);
			if(bestValue >=beta){
				break;
			}

		}

		return bestMove;
	}
	
	public void setLocalVariables(Engine engine) {

		this.playerId = engine.playerId;
		this.opponentId = 1 - this.playerId;
		this.numberOfColours = engine.matchSetting.getNumColours();
		for(int i = 0; i< numberOfColours; i++){
			for(int j = 0; j<numberOfColours;j++){
				if(i>=j){
					possibleTiles.add(new Tile(i,j));
				}
			}
		}
	}
	
	/**
	 * Make move includes all affects to the state of the game not just the
	 * placement of a tile on the board
	 */
	public void makeMove(Move child, SearchState gameState) {

		try {
	
			gameState.getBoard().makeMove(child);
			gameState.getRacks().get(gameState.getCurrentPlayer()).remove(child.getTile());

			
			gameState.getScores().updateScore(playerId, gameState.getBoard());
			gameState.setCurrentPlayer(1 - gameState.getCurrentPlayer());
		} catch (ClassCastException e) {
			throw new RuntimeException(
					"SearchState not compatible with minimax enhancements");
		}

	}

	public void unmakeMove(Move child, SearchState gameState) {
		try {
	
			gameState.setCurrentPlayer(1 - gameState.getCurrentPlayer());
			
			gameState.getBoard().undoMove();
			gameState.getRacks().get(gameState.getCurrentPlayer()).add(child.getTile());

			gameState.getScores().undoScore(gameState.getCurrentPlayer(), gameState.getBoard());
		} catch (ClassCastException e) {
			throw new RuntimeException(
					"SearchState not compatible with minimax enhancements");
		}
	}

	
	private double chanceNode(SearchState state, int depth, double alpha,double beta, boolean isMax){
		if(state.getBoard().full() || depth == this.searchDepth){
			return evaluator.evaluate(state.getScores(), state.getBoard(), state.getRacks(), state.getCurrentPlayer());
		}
		
		int numberOfPossibleTiles = this.calcPossibleTiles(state.getBoard().getNumColours());
		double sum = 0;
		//System.out.println("Number of possible tiles :"+numberOfPossibleTiles);
		for (int i =0; i< numberOfPossibleTiles;i++){
			state.getRacks().get(state.getCurrentPlayer()).add(getPossibleTile(i));
			if(isMax){
				sum = sum + MinMove(state, alpha, beta,depth+1);
			}else{
				sum = sum + MaxMove(state, alpha, beta,depth+1);
			}
			
			state.getRacks().get(state.getCurrentPlayer()).remove(getPossibleTile(i));
			
		}
		double weightedAverage = sum/numberOfPossibleTiles;
		
		return weightedAverage;
	}
	
	public double MaxMove(SearchState state,double alpha,double beta, int depth) {

		if (state.getBoard().full() || this.searchDepth == depth) {
			return evaluator.evaluate(state.getScores(), state.getBoard(), state.getRacks(),this.playerId);
		}

		double bestValue =  Double.MIN_VALUE+1;

	//	System.out.println("Racksize at max node "+state.getRacks().get(state.getCurrentPlayer()).size());
		System.out.println("Current Player"+state.getCurrentPlayer());
		for(Tile t : state.getRacks().get(state.getCurrentPlayer()) ){
			System.out.println(t);
		}
		ArrayList<Move> moves = state.generateMoves();
		for (Move child : moveOrder(moves,state)) {
			this.preSearchStateAdjustment(state);

			makeMove(child, state);
			double value = chanceNode(state,depth +1,alpha,beta, true );
			unmakeMove(child, state);

			this.postSearchStateAdjustment(state);

			if (bestValue < value) {
				bestValue = value;
			}
			
			alpha = Math.max(alpha, bestValue);
			if(bestValue >=beta){
				break;
			}
		}

		return bestValue;
	}

	public double MinMove(SearchState state,double alpha,double beta, int depth) {

		if (state.getBoard().full() || this.searchDepth == depth) {
			return evaluator.evaluate(state.getScores(), state.getBoard(), state.getRacks(),this.playerId);
		}

		double bestValue =  Double.MAX_VALUE;

	//	System.out.println("Racksize at min node "+state.getRacks().get(state.getCurrentPlayer()).size());
		ArrayList<Move> moves = state.generateMoves();

		for (Move child : moveOrder(moves,state)) {
			this.preSearchStateAdjustment(state);

			makeMove(child, state);
			double value = chanceNode(state,depth +1,alpha,beta, false);
			unmakeMove(child, state);

			this.postSearchStateAdjustment(state);

			if (bestValue > value) {
				bestValue = value;
			}
			
			beta = Math.min(beta, bestValue);
			if(bestValue <=alpha){
				break;
			}

		}

		return bestValue;
	}
	
	public static void main(String [] args){

		
	}
}
