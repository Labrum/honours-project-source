package minimax;

import java.util.ArrayList;
import java.util.Collections;

import core.Move;
import engines.MoveValueComparator;

public class StandardMinimaxMC extends MiniMax {
	
	public StandardMinimaxMC(int depth){
		this.searchDepth = depth;
	}
	@Override
	public ArrayList<Move> moveOrder(ArrayList<Move> moves, SearchState state){
	//	Collections.sort(moves,new MoveValueComparator(state.getScores(),state.getBoard()));
		return moves;
	}
	
}
