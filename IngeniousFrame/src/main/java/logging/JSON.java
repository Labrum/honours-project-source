package logging;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.Gson;

import core.MatchSetting;
import core.Variant;

public class JSON {
	
	private Gson gson;
	private PrintStream output = null;
	private File file;
	private String location = "";
	public static String VARIANT_EXT = ".var";
	public static String MATCH_EXT = ".mth";
	
	public JSON(String fileLocation){
		gson = new Gson();
		location = fileLocation;
	}
	
	public void writeMatchSettings(MatchSetting match){
		file = new File(location+match.getName()+MATCH_EXT);
		try {
			file.createNewFile();
			output = new PrintStream(file);
			output.println(gson.toJson(match));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void writeVariantSettings(Variant variant){
		file = new File(location+variant.getName()+VARIANT_EXT);
		System.out.println(file.getName());
		try {
			file.createNewFile();
			output = new PrintStream(file);
			output.println(gson.toJson(variant));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Variant readVariantSettings(String name) throws IOException{
		File file = new File(name+VARIANT_EXT);
		byte[] encoded = Files.readAllBytes(Paths.get(name));
		String jsonObject =  new String(encoded, StandardCharsets.UTF_8);
		return gson.fromJson(jsonObject,Variant.class);
		
	}
	
	public MatchSetting readMatchSettings(String name) throws IOException{
		File file = new File(name);
		byte[] encoded = Files.readAllBytes(Paths.get(name));
		String jsonObject =  new String(encoded, StandardCharsets.UTF_8);
		return gson.fromJson(jsonObject,MatchSetting.class);
		
	}
}
