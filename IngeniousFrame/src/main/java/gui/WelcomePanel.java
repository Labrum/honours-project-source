package gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.jgoodies.forms.factories.DefaultComponentFactory;

import net.miginfocom.swing.MigLayout;
import core.Manager;
import java.awt.Color;

public class WelcomePanel implements Panel {

	public JPanel panel;
	private Manager manager = null;
	private JLabel heading = new JLabel();
	private JTextField textField;
	private JTextField textField_1;
	private JLabel lblPlayerName;
	private JButton btnConnect;
	private JLabel lblPortNumber;
	private JTextField textField_2;
	private JButton btnVariantEditor;
	private JLabel lblError;

	public JPanel getPanel(){
		return panel;
	}

	private void init() {
		panel = new JPanel();
		panel.setBounds(12, 26, 370, 224);
		panel.setLayout(new MigLayout("", "[49.00px][][121.00,grow][][]", "[19px][19px][][][][][][37.00]"));
		heading.setFont(new Font("Liberation Sans", Font.BOLD, 16));
		heading.setText("Ingenious!");
		panel.add(heading, "cell 2 0,alignx left,aligny center");
		
		JLabel lblHostNamejgoodiesLabel = DefaultComponentFactory.getInstance().createLabel("Host Name");
		panel.add(lblHostNamejgoodiesLabel, "cell 1 2,alignx trailing");
		
		textField = new JTextField();
		panel.add(textField, "cell 2 2,growx,aligny center");
		textField.setColumns(10);
		textField.setText("localhost");
		
		lblPortNumber = new JLabel("Port Number");
		panel.add(lblPortNumber, "cell 1 3");
		
		textField_1 = new JTextField();
		panel.add(textField_1, "cell 2 3,growx");
		textField_1.setColumns(10);
		
		lblPlayerName = DefaultComponentFactory.getInstance().createLabel("Player Name");
		panel.add(lblPlayerName, "cell 1 4,alignx trailing");
		
		textField_2 = new JTextField();
		panel.add(textField_2, "cell 2 4,growx");
		textField_2.setColumns(10);
		
		lblError = new JLabel("");
		lblError.setForeground(Color.RED);
		panel.add(lblError, "cell 2 6");
		
		btnConnect = new JButton("Connect");
		panel.add(btnConnect, "cell 1 7");
		
		btnVariantEditor = new JButton("Variant Editor");
		panel.add(btnVariantEditor, "cell 2 7");

	}

	public WelcomePanel(Manager manager) {
		this.manager = manager;
		init();

	}

	public void addLogic(){
		connectLogic();
		variantLogic();
	}
	
	public void connectLogic() {
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (arg0.getSource().equals(btnConnect)) {
					try {
						String hostName = textField.getText();
						String playerName = textField_2.getText();
						int port = Integer.parseInt(textField_1.getText());
						
						String connectionResult = manager.connect(hostName,port, playerName);
						
						if(!connectionResult.equals("Connected")){
							lblError.setText(connectionResult);
							
						}else{

							ManagerMenuPanel menuPanel = new ManagerMenuPanel(manager);
							manager.swapPanel(menuPanel);
								
						}
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});
	}
	
	
	public void variantLogic() {
		btnVariantEditor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg1) {

				if (arg1.getSource().equals(btnVariantEditor)) {
					try {
						VariantCreaterPanel variantPanel = new VariantCreaterPanel(manager);
						manager.swapPanel(variantPanel);
						
					} catch (Exception e) {
						System.out.println(e);

						System.exit(0);
					}
				}
			}
		});
	}
}
