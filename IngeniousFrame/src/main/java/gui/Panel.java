package gui;

import javax.swing.JPanel;

/**
 * Panel interface allows the engine gui to use
 * a panel without knowing implementation details
 * @author steven
 *
 */
public interface Panel {
	public JPanel getPanel();
	public void addLogic();
}
