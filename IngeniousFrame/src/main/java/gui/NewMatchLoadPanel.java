package gui;

import javax.swing.JButton;
import javax.swing.JPanel;

import core.Manager;
import core.MatchSetting;
import core.Variant;
import core.MatchSetting.MatchBuilder;

import javax.swing.JScrollPane;

import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TreeMap;

import net.miginfocom.swing.MigLayout;

import javax.swing.JComboBox;

public class NewMatchLoadPanel implements Panel {
	private JPanel panel;
	private Manager manager;
	private JComboBox comboBox;
	JButton createBtn;
	JButton setupCreateBtn;
	String PATH_RESOURCES = "src/main/resources/MatchSettings/Manager/";
	public NewMatchLoadPanel(Manager manager){
		this.manager = manager;
		init();
		addLogic();
	}
	

	private void init() {
		panel = new JPanel();
		panel.setBounds(12, 26, 400, 311);
		panel.setLayout(new MigLayout("", "[grow][][][][]", "[][][][][][][]"));
		
		comboBox = new JComboBox(manager.getManagerSettings());
		panel.add(comboBox, "cell 0 3,growx");
		
		createBtn = new JButton("Create Match");
		panel.add(createBtn, "cell 0 6");
		
		setupCreateBtn = new JButton("Setup & Create Match");
		panel.add(setupCreateBtn, "cell 2 6");

		
	}
	
	public void setupGameLogic() {
		setupCreateBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (arg0.getSource().equals(setupCreateBtn)) {
					try {
						NewMatchPanel matchPanel = new NewMatchPanel(manager);
						manager.swapPanel(matchPanel);

					} catch (Exception e) {
						e.printStackTrace();

						System.exit(0);
					}
				}
			}
		});
	}
	public void createGameLogic() {
		System.out.println("Create game logic launched");
		createBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (arg0.getSource().equals(createBtn)) {					
					try {
						String settingName = (String)comboBox.getSelectedItem();
						System.out.println(settingName);
						MatchSetting match = manager.unpackMatchSetting(settingName);

						if (manager.newMatch(match)) {
							TreeMap<Integer,String> managerEngines = manager.identifyManagerEngines(match.getEngineNames());
							System.out.println(managerEngines.toString());
							if(manager.launchEngines(managerEngines,match)){
								System.out.println("Engines launched");
							}
							
							System.out.println("new match message sent");
							
							panel.setVisible(false);
							manager.minimizeFrame();
							
						} else {
							System.out.println("Failure");
						}

					} catch (Exception e) {
						System.out.println("EXCEPTION : " + e);
						e.printStackTrace();
					}
				}
			}
		});
	}
	
	public JPanel getPanel() {
		// TODO Auto-generated method stub
		return this.panel;
	}

	public void addLogic() {
		// TODO Auto-generated method stub
		setupGameLogic();
		createGameLogic();
	}

}
