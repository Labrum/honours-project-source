package gui;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import core.Manager;
import engines.Engine;

public class ManagerMenuPanel implements Panel {

	public JPanel panel;
	private JButton newGameButton;
	private JButton joinGameButton;
	private Manager manager;
	private JLabel heading = new JLabel();
	private JButton createVariantButton;
	private JButton btnMatchSettings;

	public JPanel getPanel(){
		return panel;
	}

	private void init() {
		panel = new JPanel();
		panel.setBounds(12, 26, 485, 500);
		
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{49, 75, 109, 104, 0};
		gbl_panel.rowHeights = new int[]{34, 25, 0, 58, 0, 35, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		heading.setFont(new Font("Liberation Sans", Font.BOLD, 16));
		heading.setText("Ingenious!");
		
		GridBagConstraints gbc_heading = new GridBagConstraints();
		gbc_heading.anchor = GridBagConstraints.WEST;
		gbc_heading.insets = new Insets(0, 0, 5, 5);
		gbc_heading.gridx = 2;
		gbc_heading.gridy = 0;
		panel.add(heading, gbc_heading);
		newGameButton = new JButton("New Game");
		
		GridBagConstraints gbc_newGameButton = new GridBagConstraints();
		gbc_newGameButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_newGameButton.insets = new Insets(0, 0, 5, 5);
		gbc_newGameButton.gridx = 1;
		gbc_newGameButton.gridy = 4;
		panel.add(newGameButton, gbc_newGameButton);

		createVariantButton = new JButton("Create Variant");
		GridBagConstraints gbc_createVariantButton = new GridBagConstraints();
		gbc_createVariantButton.insets = new Insets(0, 0, 5, 0);
		gbc_createVariantButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_createVariantButton.gridwidth = 2;
		gbc_createVariantButton.gridx = 3;
		gbc_createVariantButton.gridy = 4;
		panel.add(createVariantButton, gbc_createVariantButton);
		
		joinGameButton = new JButton("Join Game");
		GridBagConstraints gbc_joinGameButton = new GridBagConstraints();
		gbc_joinGameButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_joinGameButton.insets = new Insets(0, 0, 5, 5);
		gbc_joinGameButton.gridx = 1;
		gbc_joinGameButton.gridy = 6;
		panel.add(joinGameButton, gbc_joinGameButton);
		
		btnMatchSettings = new JButton("Match Settings");
		btnMatchSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		GridBagConstraints gbc_btnMatchSettings = new GridBagConstraints();
		gbc_btnMatchSettings.insets = new Insets(0, 0, 5, 5);
		gbc_btnMatchSettings.gridx = 3;
		gbc_btnMatchSettings.gridy = 6;
		panel.add(btnMatchSettings, gbc_btnMatchSettings);

	}

	public ManagerMenuPanel(Manager manager) {
		this.manager = manager;
		init();
		addLogic();

	}
	
	public void addLogic(){
		joinGameLogic();
		newGameLogic();
		variantLogic();
		settingsLogic();
	}

	public void joinGameLogic() {
		joinGameButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (arg0.getSource().equals(joinGameButton)) {
					try {

						LobbyPanel lobby = new LobbyPanel(manager);
						manager.swapPanel(lobby);
					} catch (Exception e) {
						System.out.println(e+"Lobby failed");

						System.exit(0);
					}
				}
			}
		});
	}

	public void newGameLogic() {
		newGameButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (arg0.getSource().equals(newGameButton)) {
					try {
						NewMatchLoadPanel matchPanel = new NewMatchLoadPanel(manager);
						manager.swapPanel(matchPanel);

					} catch (Exception e) {
						e.printStackTrace();

						System.exit(0);
					}
				}
			}
		});
	}
	
	public void variantLogic() {
		createVariantButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (arg0.getSource().equals(createVariantButton)) {
					try {
						VariantCreaterPanel variantPanel = new VariantCreaterPanel(manager);
						manager.swapPanel(variantPanel);
						
					} catch (Exception e) {
						System.out.println(e);

						System.exit(0);
					}
				}
			}
		});
	}
	
	public void settingsLogic() {
		btnMatchSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (arg0.getSource().equals(btnMatchSettings)) {
					try {
						MatchSettingPanel settingsPanel = new MatchSettingPanel(manager);
						manager.swapPanel(settingsPanel);
						
					} catch (Exception e) {
						System.out.println(e);

						System.exit(0);
					}
				}
			}
		});
	}

}
