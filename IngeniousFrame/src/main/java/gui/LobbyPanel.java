package gui;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JList;

import java.awt.GridLayout;
import java.awt.Color;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.JLabel;

import core.Constants;
import core.Manager;
import core.ManagerEngineFactory;
import engines.Engine;
import net.miginfocom.swing.MigLayout;

public class LobbyPanel implements Panel {
	private JPanel panel;
	private JList<String> lobby;
	private JScrollPane scrollPane;
	private JButton refreshButton;
	private JButton joinButton;
	private Manager manager;
	private String[] controllers = new String[0];
	private String[] engines = new String[0];
	private JList<String> availableEngines;
	private JScrollPane scrollPane_1;

	public LobbyPanel(Manager manager) {
		this.manager = manager;
		init();
		addLogic();
	}

	public JPanel getPanel() {
		return panel;
	}

	private void init() {
		panel = new JPanel();
		panel.setBounds(12, 26, 426, 222);
		panel.setLayout(new MigLayout("", "[143px][grow]",
				"[15px][124px,grow][25px]"));

		JLabel lblLobby = new JLabel("New Lobby Page");
		panel.add(lblLobby, "cell 0 0,alignx right,aligny top");

		scrollPane = new JScrollPane();
		panel.add(scrollPane, "cell 0 1,grow");

		lobby = new JList<String>(controllers);
		scrollPane.setViewportView(lobby);

		scrollPane_1 = new JScrollPane();
		panel.add(scrollPane_1, "cell 1 1,grow");

		engines = combineArrays(Constants.FacilitatorPlayers,
				manager.getManagerPlayers());
		availableEngines = new JList<String>(engines);

		scrollPane_1.setViewportView(availableEngines);

		joinButton = new JButton("Join Game");
		panel.add(joinButton, "flowx,cell 0 2,growx,aligny top");

		refreshButton = new JButton("Refresh");
		refresh();

		panel.add(refreshButton, "cell 0 2");
		panel.setVisible(true);
	}

	public void refreshLogic() {
		refreshButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (arg0.getSource().equals(refreshButton)) {
					refresh();
				}
			}
		});
	}

	private void refresh() {
		try {
			controllers = manager.refreshLobby();
			lobby.setListData(controllers);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void joinLogic() {
		joinButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (arg0.getSource().equals(joinButton)) {
					try {
						System.out.println("Join game called");
						String name = lobby.getSelectedValue();
						int port = manager.controllerPort(name);

						ManagerEngineFactory factory = new ManagerEngineFactory(
								"localhost", port);
						
						factory.generateEngine(availableEngines.getSelectedValue(),manager.requestMatchSetting(name), 1).run();

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	private static String[] combineArrays(String[] first, String[] second) {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < first.length; i++) {
			ret.add(first[i]);
		}
		for (int i = 0; i < second.length; i++) {
			ret.add(second[i]);
		}
		return ret.toArray(new String[ret.size()]);
	}

	public void addLogic() {
		refreshLogic();
		joinLogic();
	}
}
