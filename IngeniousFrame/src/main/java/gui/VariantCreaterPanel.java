package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintStream;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import javax.swing.JSpinner;
import javax.swing.JLabel;

import com.google.gson.Gson;
import com.jgoodies.forms.factories.DefaultComponentFactory;

import core.Manager;
import core.Variant;
import core.Variant.VariantBuilder;
import engines.Engine;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JSlider;
import javax.swing.JList;
import javax.swing.JTextField;

import logging.JSON;

public class VariantCreaterPanel implements Panel {
	public JPanel panel;

	private JTextField variantName;
	private JLabel label;
	private JLabel lblMaxColourScore;
	private JSpinner maxColour;
	private JSpinner winningScore;
	private JButton btnCreate;
	private JLabel lblRackSize;
	private JSpinner rackSize;
	private Variant gameRules;
	private Manager manager;
	String PATH_RESOURCES = "src/main/resources/Variants/Manager/";
	private JCheckBox chckbxBagVisible;
	private JCheckBox chckbxOpRack;
	private JCheckBox chckbxSwapAllowed;
	private JCheckBox chckbxOutrightWinAllowed;


	public VariantCreaterPanel(Manager manager) {
		this.manager = manager;
		init();
		addLogic();
	}

	public JPanel getPanel() {
		return panel;
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	private void init() {
		panel = new JPanel();
		panel.setBounds(12, 26, 400, 404);

		JLabel lblMatchName = new JLabel("Edit variant");
		JLabel lblPlayerName = new JLabel("Variant Name");
		panel.setLayout(new MigLayout("", "[129px][51px][161px]", "[15px][15px][30px][15px][19px][15px][19px][][][25px]"));
		panel.add(lblMatchName, "cell 0 1,growx,aligny top");

		panel.add(lblPlayerName, "cell 2 1,growx,aligny top");

		variantName = new JTextField();
		panel.add(variantName, "cell 2 2,alignx left,aligny top");
		variantName.setColumns(10);
		
		chckbxBagVisible = new JCheckBox("Bag Visible");
		panel.add(chckbxBagVisible, "cell 0 5");
		
		chckbxOpRack = new JCheckBox("Opponent Rack Visible");
		panel.add(chckbxOpRack, "cell 0 6");
		
		chckbxSwapAllowed = new JCheckBox("Swap Allowed");
		panel.add(chckbxSwapAllowed, "cell 0 7");
		
		chckbxOutrightWinAllowed = new JCheckBox("Outright win allowed");
		panel.add(chckbxOutrightWinAllowed, "cell 2 7");

		btnCreate = new JButton("Create");
		panel.add(btnCreate, "cell 0 9 3 1,alignx center,aligny top");

		label = new JLabel("Create new Variant");
		panel.add(label, "cell 0 0 3 1,alignx center,aligny top");

		lblMaxColourScore = new JLabel("Max Colour Score");
		panel.add(lblMaxColourScore, "cell 0 3,alignx left,aligny top");

		maxColour = new JSpinner();
		panel.add(maxColour, "cell 0 4,grow");

		JLabel lblWinningScore = new JLabel("Winning Score");
		panel.add(lblWinningScore, "cell 2 3,growx,aligny top");

		winningScore = new JSpinner();
		panel.add(winningScore, "cell 2 4,grow");

		lblRackSize = new JLabel("Rack size");
		panel.add(lblRackSize, "cell 2 5,alignx left,aligny top");

		rackSize = new JSpinner();
		panel.add(rackSize, "cell 2 6,grow");

		JComboBox comboBox = new JComboBox(manager.getManagerVariants());
		panel.add(comboBox, "cell 0 2,grow");

	}
	
	public void createVariantLogic() {
		System.out.println("Create variant");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VariantBuilder varBuild = new VariantBuilder(variantName.getText(),0,
						(Integer) maxColour.getValue(),
						(Integer) winningScore.getValue(),
						(Integer) rackSize.getValue());
				varBuild.setOutrightWin(chckbxOutrightWinAllowed.isSelected());
				varBuild.setOutrightWin(chckbxOutrightWinAllowed.isSelected());
				varBuild.setSwap(chckbxSwapAllowed.isSelected());
				varBuild.setOppRackSizeVisible(chckbxOpRack.isSelected());
				
				Variant variant = varBuild.createVariant();
				
				JSON jsonWriter = new JSON(PATH_RESOURCES);
				jsonWriter.writeVariantSettings(variant);
			}
		});
	}

	public void addLogic() {
		createVariantLogic();

	}
}
