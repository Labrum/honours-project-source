package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import logging.JSON;
import net.miginfocom.swing.MigLayout;
import core.Constants;
import core.Manager;
import core.MatchSetting;
import core.Variant;
import core.MatchSetting.MatchBuilder;
import core.Variant.VariantBuilder;

public class MatchSettingPanel implements Panel {

	private JPanel panel;	
	private JButton btnCreate;
	private JButton btnExchange;
	private JButton btnExchangeVariant;	
	private JSpinner numPlayerSpinner;
	private JSpinner currentColour;	
	private JSpinner boardSize = new JSpinner();

	private Manager manager;	
	private String[] matchColours;
	private String[] matchPlayers = new String[0];
	private String[] variants = new String[0];	
	private MatchBuilder matchBuilder = null;	

	private JList<String> matchEngines;
	private JList<String> availableEngines;
	private JList<String> matchVariants;
	private JList<String> availableVariants;
	private JTextField txtLobbyname;
	
	String PATH_RESOURCES = "src/main/resources/MatchSettings/Manager/";

	/**
	 * Create the panel.
	 */
	public MatchSettingPanel(Manager manager) {
		this.manager = manager;
		init();
		addLogic();
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public void init() {

		panel = new JPanel();

		panel.setLayout(new MigLayout("", "[170.00,grow][][219.00,grow]",
				"[][][][][][][][22.00][][25.00][125.00,grow][][grow][][]"));

		final JLabel lblTiles = new JLabel("Tiles");
		JLabel lblLobbyName = new JLabel("Lobby Name");
		JLabel lblNumberOfPlayers = new JLabel("Number of players");
		JLabel lblWinningScoreLabel = new JLabel("Winning Score");
		JLabel lblBoardSize = new JLabel("Board Size");
		JLabel lblBagSize = new JLabel("Bag Size");
		JLabel lblNewLabel = new JLabel("Number of Colours");
		JLabel lblRatioOfClours = new JLabel("Ratio of colours");
		JLabel lblJoinGame = new JLabel("Join Game");
		JLabel lblStartingPlayers = new JLabel("Starting Players");
		JLabel lblAddPlayers = new JLabel("Add players");
		JLabel lblAvailableVariant = new JLabel("Available variant");
		JLabel lblEngineVariant = new JLabel("Engine variant");

		JScrollPane scrollPane = new JScrollPane();
		JScrollPane scrollPane_1 = new JScrollPane();
		JScrollPane scrollPane_2 = new JScrollPane();
		JScrollPane scrollPane_3 = new JScrollPane();
		JScrollPane scrollPane_4 = new JScrollPane();

		JSpinner winningScore = new JSpinner();
		JSpinner bagSize = new JSpinner();

		txtLobbyname = new JTextField();
		txtLobbyname.setText("LobbyName");
		numPlayerSpinner = new JSpinner();

		currentColour = new JSpinner();
		
		availableEngines = new JList<String>();
		matchEngines = new JList<String>(matchPlayers);
		btnExchange = new JButton("Exchange");
		matchVariants = new JList();
		availableVariants = new JList();
		btnExchangeVariant = new JButton("Exchange");
		btnCreate = new JButton("Create Match Setting");

		winningScore.setValue(18);
		boardSize.setValue(11);
		bagSize.setValue(120);

		scrollPane_1.setViewportView(availableEngines);
		scrollPane_2.setViewportView(matchEngines);
		scrollPane_3.setViewportView(availableVariants);
		scrollPane_4.setViewportView(matchVariants);

		matchEngines.setListData(combineArrays(Constants.FacilitatorPlayers,
				manager.getManagerPlayers()));
		availableVariants.setListData(manager.getManagerVariants());

		panel.add(lblLobbyName, "cell 0 0");

		panel.add(txtLobbyname, "cell 2 0,growx");
		txtLobbyname.setColumns(10);

		panel.add(lblNumberOfPlayers, "cell 0 1");

		panel.add(numPlayerSpinner, "cell 2 1,growx");

		panel.add(lblWinningScoreLabel, "cell 0 2");

		panel.add(winningScore, "cell 2 2,growx");

		panel.add(lblBoardSize, "cell 0 3");

		panel.add(boardSize, "cell 2 3,growx");

		panel.add(lblBagSize, "cell 0 4");

		panel.add(bagSize, "cell 2 4,growx");

		panel.add(lblNewLabel, "cell 0 5,alignx left");

		panel.add(currentColour, "cell 2 5,growx");

		panel.add(lblStartingPlayers, "cell 0 9,grow");

		panel.add(lblAddPlayers, "cell 2 9");

		panel.add(scrollPane_1, "cell 0 10,grow");

		panel.add(scrollPane_2, "cell 2 10,grow");

		panel.add(lblEngineVariant, "cell 0 11");

		panel.add(btnExchange, "cell 1 11");

		panel.add(lblAvailableVariant, "cell 2 11");

		panel.add(scrollPane_4, "cell 0 12,grow");

		panel.add(scrollPane_3, "cell 2 12,grow");

		panel.add(btnExchangeVariant, "cell 1 13");

		panel.add(btnCreate, "cell 0 14");

		currentColour.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				Integer value = (Integer) currentColour.getValue();
				if (value > 0) {
					matchColours = new String[value];
					matchColours = Arrays.copyOf(Constants.Colours, value);
				}
			}
		});

		numPlayerSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				Integer value = (Integer) numPlayerSpinner.getValue();
				if (value > 0) {
					ArrayList<String> temp1 = new ArrayList<String>();
					ArrayList<String> temp2 = new ArrayList<String>();
					for (int i = 0; i < value - 1; i++) {
						temp1.add(matchPlayers[i]);
						temp2.add(variants[i]);
					}
					temp1.add("Open_slot");
					temp2.add("default_variant");

					availableEngines.setListData(temp1.toArray(new String[temp1
							.size()]));
					matchVariants.setListData(temp2.toArray(new String[temp2
							.size()]));
					matchPlayers = (String[]) temp1.toArray(
							new String[temp1.size()]).clone();
					variants = (String[]) temp2.toArray(
							new String[temp2.size()]).clone();
				}
			}
		});
	}

	private static String[] combineArrays(String[] first, String[] second) {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < first.length; i++) {
			ret.add(first[i]);
		}
		for (int i = 0; i < second.length; i++) {
			ret.add(second[i]);
		}
		return ret.toArray(new String[ret.size()]);
	}
	public void exchangeVariantLogic() {
		System.out.println("Exchange Variant launched");
		btnExchangeVariant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg1) {
				if (arg1.getSource().equals(btnExchangeVariant)) {
					try {
						if (!matchVariants.isSelectionEmpty()
								|| !availableVariants.isSelectionEmpty()) {
							System.out.println(availableVariants.getModel()
									.toString());
							System.out.println(matchVariants.getModel()
									.toString());

							variants[matchVariants.getSelectedIndex()] = availableVariants
									.getSelectedValue();

							for (int i = 0; i < matchPlayers.length; i++) {
								System.out.println(variants[i]);
							}

							matchVariants.setListData(variants);

						} else {
							System.out.println("Selection empty");
							JOptionPane
									.showMessageDialog(
											panel,
											"Ensure that a player slot and an engine has been\n selected to exchange.",
											"Exchange error",
											JOptionPane.ERROR_MESSAGE);

						}
					} catch (Exception e) {

						JOptionPane
								.showMessageDialog(
										panel,
										"Ensure that a player slot and an engine has been\n selected to exchange.",
										"Exchange error",
										JOptionPane.ERROR_MESSAGE);
					}

				}

			}
		});
	}

	public void exchangeEngineLogic() {
		System.out.println("Exchange logic launched");
		btnExchange.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg1) {
				if (arg1.getSource().equals(btnExchange)) {
					try {
						if (!matchEngines.isSelectionEmpty()
								|| !availableEngines.isSelectionEmpty()) {
							System.out.println(availableEngines.getModel()
									.toString());
							System.out.println(matchEngines.getModel()
									.toString());

							matchPlayers[availableEngines.getSelectedIndex()] = matchEngines
									.getSelectedValue();

							for (int i = 0; i < matchPlayers.length; i++) {
								System.out.println(matchPlayers[i]);
							}

							availableEngines.setListData(matchPlayers);

						} else {
							System.out.println("Selection empty");
							JOptionPane
									.showMessageDialog(
											panel,
											"Ensure that a player slot and an engine has been\n selected to exchange.",
											"Exchange error",
											JOptionPane.ERROR_MESSAGE);

						}
					} catch (Exception e) {
						JOptionPane
								.showMessageDialog(
										panel,
										"Ensure that a player slot and an engine has been\n selected to exchange.",
										"Exchange error",
										JOptionPane.ERROR_MESSAGE);
					}

				}

			}
		});
	}

	public JPanel getPanel() {

		return this.panel;
	}

	public void addLogic() {
		exchangeEngineLogic();
		exchangeVariantLogic();
		createSettingsLogic();
	}
	public void createSettingsLogic() {
		System.out.println("Create Match Setting");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				matchBuilder = new MatchBuilder(txtLobbyname.getText(), (Integer) numPlayerSpinner.getValue(),	matchColours.length);
				matchBuilder.setEngines(matchPlayers);
				Variant[] vars = manager.unpackVariants(variants);
				matchBuilder.setVariants(vars);
				matchBuilder.setBoardSize((Integer)boardSize.getValue());
				MatchSetting match = matchBuilder.buildMatch();
				JSON jsonWriter = new JSON(PATH_RESOURCES);
				jsonWriter.writeMatchSettings(match);
			}
		});
	}
	
}
