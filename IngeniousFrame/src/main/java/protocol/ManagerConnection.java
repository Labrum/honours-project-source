package protocol;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.TreeMap;

import core.Constants;
import core.Controller;
import core.Facilitator;
import core.FacilitatorEngineFactory;
import core.ManagerEngineFactory;
import core.MatchSetting;
import core.Variant;

public class ManagerConnection implements Runnable {

	private Socket client = null;
	private Facilitator server = null;
	private ObjectOutputStream outputStream;
	private ObjectInputStream inputStream;
	private String playerName;
	
	public ManagerConnection(Socket client, Facilitator server){
		this.client = client;
		this.server = server;
		openStreams();
		try {
			playerName = (String) inputStream.readObject();
			
			while(server.checkDuplicateName(playerName)){
				outputStream.writeObject("NameTaken");
				playerName = (String) inputStream.readObject();
			}
			outputStream.writeObject("Acknowledged");
			System.out.println(playerName);
			
			
		} catch (Exception e) {
			System.out.println("Error in receiving playername");
		}
	}
	
	public String getPlayerName(){
		return playerName;
	}
	
	
	public void run() {
		
		System.out.println("Manager connection created");
		server.addManager(playerName, this);
		
		while(true){
			try {
				String command = (String) inputStream.readObject();
				
				if(command.equals("NewMatch")){
					
					MatchSetting match = (MatchSetting) inputStream.readObject();
					System.out.println("Match received");
					Controller controller = new Controller(match);
					Thread controllerThread = new Thread(controller);
					controllerThread.start();
					server.addLobby(match.getName(), controller);
					outputStream.writeObject(controller.getPort());
					TreeMap<Integer,String> facilitatorEngines = identifyFacilitatorEngines(match.getEngineNames());
					launchEngines(facilitatorEngines,match,controller.getPort());
					
					
					
				}else if(command.equals("Refresh")){
					outputStream.writeObject(server.getControllerNames());
				}else if(command.equals("ControllerPort")){
					String controllerName = (String) inputStream.readObject();
					outputStream.writeObject(server.getControllerPort(controllerName));
				}else if(command.equals("MatchSetting")){
					String controllerName = (String) inputStream.readObject();
					outputStream.writeObject(server.getMatchSettings(controllerName));
				}
				else{
					System.out.println(command);
				}
			} catch (Exception e) {
				System.out.println("Appropriate command not received");
				break;
			}
		}
		
	}

	private boolean launchEngines(TreeMap<Integer, String> engines,MatchSetting match,int controllerPort){

		FacilitatorEngineFactory factory = new FacilitatorEngineFactory("localhost", controllerPort);
		for(int i : engines.keySet()){
			System.out.println(engines.get(i));
			Thread thread = new Thread(factory.generateEngine(engines.get(i),match, i));
			thread.start();
		}

		return true;
	}
	
	/**
	 * Receives the list of engines chosen by a user and returns a
	 * Treemap that contains all the manager only engines. Treemap
	 * key is the position in the array.
	 * 
	 * 
	 * @param engineNames
	 * @return managerOnlyEngines
	 */
	private TreeMap<Integer,String> identifyFacilitatorEngines(String[] engineNames){
		
		TreeMap<Integer,String> engines = new TreeMap<Integer,String>();
		
		for(int i =0; i< engineNames.length;i++){
			for(String FacilitatorEngine : Constants.FacilitatorPlayers){
				if(engineNames[i].equals(FacilitatorEngine)){
					engines.put(i, engineNames[i]);
					break;
				}
			}
		}

		return engines;
	}
	
	/**
	 * Opens IO-Streams catches and prints failures
	 */
	public void openStreams() {
		try {
			outputStream = new ObjectOutputStream(this.client.getOutputStream());
		} catch (IOException e) {
			System.out.println("Output Stream failed to open.");
		}
		try {
			inputStream = new ObjectInputStream(this.client.getInputStream());
		} catch (IOException e) {
			System.out.println("Input Stream failed to open.");
		}
	}

	/**
	 * Disconnects IO-streams and closes socket catches and prints failures
	 */
	public void closeConnection() {
		try {
			inputStream.close();
		} catch (IOException e) {
			System.out.println("Input stream failed to close");
		}

		try {
			outputStream.close();
		} catch (IOException e) {
			System.out.println("Input stream failed to close");
		}
		
		try {
			client.close();
		} catch (IOException e) {
			System.out.println("Socket failed to close");
		}
	}
}
