package protocol;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import core.Controller;
import core.Coordinate;
import core.Move;
import core.Rack;
import core.Tile;
import core.Variant;

public class EngineConnection {

	private Controller controller;
	private Socket client;
	private String playerName;
	private MessageHandler messageHandler;
	public int id;

	public EngineConnection(Controller controller, Socket client) {
		this.controller = controller;
		this.client = client;

		messageHandler = new MessageHandler(client);
		messageHandler.openStreams();
		messageHandler.command("id");
		try {

			String msg[] = messageHandler.receiveMessage();
			this.id = Integer.parseInt(msg[1]);

			messageHandler.command("name");
			msg = messageHandler.receiveMessage();

			this.playerName = msg[1];
			System.out.println(playerName);

			controller.addEngine(this, this.id);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String toString() {
		return playerName;
	}

	public void playMove(Move move) {
		messageHandler.command("playmove", move.getTile().getTopColour() + "",
				move.getTile().getBottomColour() + "", move.getTile()
						.getRotation() + "", move.getPosition().getX() + "",
				move.getPosition().getY() + "");
	}

	public void setRack(Rack rack, int playerId) {
		System.out.println("SET RACK CALLED");
		String[] params = new String[rack.capacity() * 2 + 1];
		params[0] = playerId + "";
		int i = 1;
		for (Tile tile : rack) {
			params[i] = tile.getTopColour() + "";
			params[i + 1] = tile.getBottomColour() + "";
			i = i + 2;
		}

		messageHandler.command("setrack", params);
	}

	public void setBag(Tile[] bag) {
		String[] params = new String[bag.length * 2];
		
		int i = 0;
		for (Tile tile : bag) {
			params[i] = tile.getTopColour() + "";
			params[i + 1] = tile.getBottomColour() + "";
			i = i + 2;
		}
		messageHandler.command("setbag", params);
	}

	public void draw(Tile tile, int player) {
		messageHandler.command("draw", player + "", tile.getTopColour() + "",
				tile.getBottomColour() + "");
	}

	public Move genMove() {
		messageHandler.command("genmove");

		String[] moveReply;

		try {
			moveReply = messageHandler.receiveMessage();
			System.out.println(moveReply.length + " LENGTH OF REPLY");
		} catch (Exception e) {
			return null;
		}

		if (moveReply[0].equals("=")) {
			Tile tile = new Tile(Integer.parseInt(moveReply[1]),
					Integer.parseInt(moveReply[2]));
			tile.setRotation(Integer.parseInt(moveReply[3]),
					controller.NUMBER_OF_COLOURS);
			Coordinate coords = new Coordinate(Integer.parseInt(moveReply[4]),
					Integer.parseInt(moveReply[5]));
			return new Move(tile, coords);
		}

		return null;
	}
}
