package protocol;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;

import core.Move;

/**
 * MessageManager reads/writes input from streams
 * and abstracts reused code. MessageManager will
 * do the string manipulation needed to encode and
 * decode Go text-protocol messages.
 * 
 * @author steven
 *
 */
public class MessageHandler {
	
	private PrintStream outputStream;
	private BufferedReader inputStream;
	private Socket client = null;
	
	public MessageHandler(Socket client){
		this.client = client;
		openStreams();
	}
	
	public String [] splitMessage(String message){
		
		String messageValues[] = message.split(" ");
		String messageType = messageValues[0];
		String commandParameters[] = new String[1];
		
		if(messageValues.length==2){
			commandParameters = messageValues[1].split(",");
		}	
		String returnString[] = new String[commandParameters.length+1];
		returnString[0] = messageType;
		int count = 1;
		for(String s : commandParameters){
			returnString[count++] = s;
		}
		return returnString;
	}
	
	public String[] receiveMessage() throws Exception{
		String message = inputStream.readLine();
		
		String messageValues[] = message.split(" ");
		String messageType = messageValues[0];
		String commandParameters[] = new String[1];
		
		if(messageValues.length==2){
			commandParameters = messageValues[1].split(",");
		}	
		String returnString[] = new String[commandParameters.length+1];
		returnString[0] = messageType;
		int count = 1;
		for(String s : commandParameters){
			returnString[count++] = s;
		}
		for(int i =0; i< returnString.length;i++){
		//	System.out.println("MESSAGE RECEIVED : "+returnString[i]);
		}
		return returnString;
	}
	public void sendString(String message){
		outputStream.println(message);
	}
	
	public void command(String command, String... params){
		String message = command+" ";
		
		for(int i =0; i< params.length-1;i++){
			message= message+params[i]+",";
		}
		if(params.length>0){
			message = message + params[params.length-1];
		}
		outputStream.println(message);		
	}
	
	public void reply(String... params){
		command("=",params);
	}
	
	public void reply(Move move){
		if(move == null){
			errorReply("Null-move");
		}
		command("=",move.getTile().getTopColour() + "",
				move.getTile().getBottomColour() + "", move
				.getTile().getRotation() + "", move
				.getPosition().getX() + "", move
				.getPosition().getY() + "");
	}
	
	public void replyBrute(String reply){
		outputStream.println(reply);
	}
	
	public void errorReply(String... params){
		command("?",params);
	}

	/**
	 * Opens IO-Streams catches and prints failures
	 */
	public void openStreams() {
		try {
			outputStream = new PrintStream(this.client.getOutputStream(),true);
		} catch (IOException e) {
			System.out.println("Output Stream failed to open.");
		}
		try {
			inputStream = new BufferedReader(new InputStreamReader(
					this.client.getInputStream()));
		} catch (IOException e) {
			System.out.println("Input Stream failed to open.");
		}
	}

	/**
	 * Disconnects IO-streams and closes socket catches and prints failures
	 */
	public void closeConnection() {
		try {
			inputStream.close();
		} catch (IOException e) {
			System.out.println("Input stream failed to close");
		}
		outputStream.flush();
		outputStream.close();
		try {
			client.close();
		} catch (IOException e) {
			System.out.println("Socket failed to close");
		}
	}
	
}
