package protocol;
import java.io.Serializable;


public class Message implements Serializable{
	/**
	 * serial
	 */
	private static final long serialVersionUID = -4448274655106622792L;
	private String text;
	
		public Message(String msg){
			text = msg;
		}
		
		public String toString(){
			return text;
		}
}