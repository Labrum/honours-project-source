package engines;
import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JScrollPane;
import javax.swing.JLayeredPane;
import javax.swing.JList;

import protocol.Message;
import protocol.MessageHandler;

public class HumanPlayer {

	private JFrame frame;
	private Socket socket = null;
	private PrintWriter outputStream = null;
	private BufferedReader inputStream = null;
	private int port;
	private String host;
	private JButton btnNewGameButton;
	private JButton btnJoinGame;
	private JPanel panel = new JPanel();
	private JList list;
	private JPanel panel2 = new JPanel();
	private JLayeredPane layeredPane;
	private JScrollPane scrollPane = new JScrollPane();
	private MessageHandler msgHandle = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HumanPlayer window = new HumanPlayer();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HumanPlayer() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		panel.setBounds(12, 26, 426, 222);
		frame.getContentPane().add(panel);
		panel2.setBounds(12, 26, 426, 222);
		panel2.setVisible(false);
		frame.getContentPane().add(panel2);
		// panel2.setLayout(null);

		btnNewGameButton = new JButton("New Game");
		panel.add(btnNewGameButton);

		btnJoinGame = new JButton("Join Game");
		panel.add(btnJoinGame);

		layeredPane = new JLayeredPane();
		layeredPane.setBounds(0, 0, 1, 1);
		frame.getContentPane().add(layeredPane);

		btnJoinGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (arg0.getSource().equals(btnJoinGame)) {
					try {
						panel.setVisible(false);
						panel2.setVisible(true);

						socket = new Socket("localhost", 3003);
						openStreams();

						scrollPane.setBounds(0, 130, 220, -129);
						panel2.add(scrollPane);

						scrollPane.setViewportView(list);
						
						closeConnection();
						

					} catch (Exception e) {
						System.out.println(e);

						System.out.println("Client could not connect to" + host
								+ " and port: " + port);
						System.exit(0);
					}
				}
			}
		});

		btnNewGameButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (arg0.getSource().equals(btnNewGameButton)) {
					try {
						socket = new Socket("localhost", 3003);
						msgHandle = new MessageHandler(socket);
						
						String s = JOptionPane.showInputDialog("Select a lobby name");

						msgHandle.command("NewGame",s);
						
						String[] newGameReply  = msgHandle.receiveMessage();
						String PORT_TO_BE_RECEIVED = null;
						if(newGameReply[0].equals("=")){
							PORT_TO_BE_RECEIVED= newGameReply[1];
							
						}
						
						System.out.println(PORT_TO_BE_RECEIVED);
					
						msgHandle.closeConnection();
						
						socket = new Socket("localhost", Integer.parseInt(PORT_TO_BE_RECEIVED));
						openStreams();

						s = JOptionPane.showInputDialog("Select a player name");
						outputStream.write(s);
						panel.setVisible(false);
					} catch (Exception e) {
						System.out.println(e);

						System.out.println("Client could not connect to" + host	+ " and port: " + port);
						System.exit(0);
					}
				}
			}
		});
	}

	/**
	 * Opens IO-Streams catches and prints failures
	 */
	public void openStreams() {
		try {
			outputStream = new PrintWriter(this.socket.getOutputStream(),true);
		} catch (IOException e) {
			System.out.println("Output Stream failed to open.");
		}
		try {
			inputStream = new BufferedReader(new InputStreamReader(
					this.socket.getInputStream(),"US_ASCII"));
		} catch (IOException e) {
			System.out.println("Input Stream failed to open.");
		}
	}

	/**
	 * Disconnects IO-streams and closes socket catches and prints failures
	 */
	public void closeConnection() {
		try {
			inputStream.close();
		} catch (IOException e) {
			System.out.println("Input stream failed to close");
		}
		outputStream.close();
		try {
			socket.close();
		} catch (IOException e) {
			System.out.println("Socket failed to close");
		}
	}
}
