package engines;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import core.BagSequence;
import core.Board;
import core.Coordinate;


public class Zobrist {
		int SEED = 17;
		Random rand = new Random(SEED);
		HashMap<Coordinate, ArrayList<Long>> zobrist = new HashMap<Coordinate,ArrayList<Long>>();
		int DIAMETER;

		/**
		 * zobrist contains every possible placement of X and O
		 * on an MxN board. The extra value denotes that the next
		 * player to play is O.
		 * @param M
		 * @param N
		 */
		public Zobrist(int BoardBreadth,int NumberOfColours,int rackSize,int numberOfPlayers){
			
			zobrist = new HashMap<Coordinate,ArrayList<Long>>();
			int SIDE_LENGTH = DIAMETER/2 +1;
			for (int i = -SIDE_LENGTH + 1; i < SIDE_LENGTH; i++) {
				for (int j = -SIDE_LENGTH + 1; j < SIDE_LENGTH; j++) {
					Coordinate coord = new Coordinate(i,j);
					zobrist.put(coord, new ArrayList<Long>());
					for(int k = 0; k< NumberOfColours;k++){
						zobrist.get(coord).add(rand.nextLong());
					}
				}
			}
			Coordinate rackIndex = new Coordinate(BoardBreadth,BoardBreadth);
			zobrist.put(rackIndex,new ArrayList<Long>());
			for(int i = 0; i< rackSize;i++){
				for(int j = 0; j < NumberOfColours;j++){
					for(int k = 0; k<NumberOfColours;k++){
						if(k>=j){
							zobrist.get(rackIndex.add(new Coordinate(j,k))).add(rand.nextLong());
						}
					}
				}
			}
						
			this.DIAMETER = BoardBreadth;
		}
		public Zobrist(int BoardBreadth,int NumberOfColours,int rackSize,int numberOfPlayers,BagSequence bag){
			
			zobrist = new HashMap<Coordinate,ArrayList<Long>>();
			int SIDE_LENGTH = DIAMETER/2 +1;
			for (int i = -SIDE_LENGTH + 1; i < SIDE_LENGTH; i++) {
				for (int j = -SIDE_LENGTH + 1; j < SIDE_LENGTH; j++) {
					Coordinate coord = new Coordinate(i,j);
					zobrist.put(coord, new ArrayList<Long>());
					for(int k = 0; k< NumberOfColours;k++){
						zobrist.get(coord).add(rand.nextLong());
					}
				}
			}
			Coordinate rackIndex = new Coordinate(BoardBreadth,0);
			zobrist.put(rackIndex,new ArrayList<Long>());
			for(int i = 0; i< rackSize;i++){
				zobrist.get(rackIndex.add(new Coordinate(0,-i))).add(rand.nextLong());
			}
			Coordinate bagIndex = new Coordinate(BoardBreadth+1,0);
			zobrist.put(bagIndex,new ArrayList<Long>());
			for(int i = 0; i< rackSize;i++){
				zobrist.get(bagIndex.add(new Coordinate(0,-i))).add(rand.nextLong());
			}
			
			this.DIAMETER = BoardBreadth;
		}
		
		public long getZobHash(int x, int y,int colours){
			return zobrist.get(new Coordinate(x,y)).get(colours);
		}
		
		public long getOToPlay(){
			return 0l;
		}
		
		
		public static void main(String [] args){
			Zobrist zob = new Zobrist(7,1,4,2);
			System.out.println(zob.getZobHash(0, -2,0));
		}
		
	}
