package engines;

import java.util.ArrayList;

import core.Board;
import core.BoardInterface;
import core.Move;
import core.Tile;

public interface IMoveController {
	public Move generateMove(Engine engine,Evaluator evaluator);
	
}
