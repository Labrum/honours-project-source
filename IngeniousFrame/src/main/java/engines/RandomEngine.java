package engines;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;

import minimax.StandardMinimaxMC;
import core.Board;
import core.Coordinate;
import core.MatchSetting;
import core.Move;
import core.Tile;

public class RandomEngine extends Engine {

	public RandomEngine(String host, int port, MatchSetting match, int position)
			throws UnknownHostException, IOException {
		super(new Socket(host, port), match , position);
		gameBoard = new Board(match.getBoardSize(),match.getNumColours());
		this.moveController = new StandardMinimaxMC(0);
	}
	public String engineName(){
		return "Random Engine";
	}
}