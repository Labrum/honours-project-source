package engines;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import mcts.MCTS;
import minimax.StandardMinimaxMC;
import core.Board;
import core.MatchSetting;

public class MCTSEngine extends Engine {
		public MCTSEngine(String host, int port, MatchSetting match, int position)
				throws UnknownHostException, IOException {
			super(new Socket(host, port), match , position);
			gameBoard = new Board(match.getBoardSize(),match.getNumColours());
			this.moveController = new StandardMinimaxMC(2);
		}
		public String engineName(){
			return "Minimax Engine";
		}
}
