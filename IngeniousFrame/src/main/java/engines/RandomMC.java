package engines;

import java.util.ArrayList;
import java.util.Collections;

import core.Move;
import core.Tile;

public class RandomMC implements IMoveController {

	public Move generateMove(Engine engine, Evaluator evaluator) {
		ArrayList<Move> possibleMoves = engine.gameBoard.generateMoves(engine.racks.get(engine.playerId));
		Collections.shuffle(possibleMoves);
		Move move = possibleMoves.get(0);
		while(!engine.gameBoard.validMove(move)){
			System.out.println("THE VALID MOVES RETREIVED AREN'T VALID AT ALL");
			System.out.println(move);
			move =possibleMoves.remove(0);
		}
		System.out.println("CHOOSING FROM "+possibleMoves.size()+" MOVES");

		return move;
	}
}
