package engines;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import minimax.EMM;
import minimax.StandardMinimaxMC;
import core.Board;
import core.MatchSetting;

public class EMMEngine extends Engine {
	public EMMEngine(String host, int port, MatchSetting match, int position)
			throws UnknownHostException, IOException {
		super(new Socket(host, port), match , position);
		gameBoard = new Board(match.getBoardSize(),match.getNumColours());
		this.moveController = new EMM(2);
	}
	public String engineName(){
		return "Minimax Engine";
	}
}
