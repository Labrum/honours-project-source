package engines;

import java.util.Comparator;

import core.BoardInterface;
import core.Move;

public class MoveValueComparator implements Comparator<Move> {

	private ScoreKeeper scores;
	private BoardInterface board;
	public MoveValueComparator(ScoreKeeper score,BoardInterface board){
		this.scores = score.copy();
		this.board = board;
	}
	public int compare(Move o1, Move o2) {
		int firstSum=0;
		int secondSum =0;
		
		board.makeMove(o1);
		int [] firstScore = scores.calculateScore(board);
		board.undoMove();
		

		board.makeMove(o2);
		int [] secondScore = scores.calculateScore(board);
		board.undoMove();
		
		for(int i : firstScore){
			firstSum+=i;
		}
		for(int i : secondScore){
			secondSum+=i;
		}
		if(firstSum>secondSum){
			return -1;
		}else if(firstSum==secondSum){
			return 0;
		}else{
			return 1;	
		}
	}

}
