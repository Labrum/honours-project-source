package engines;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

import core.Board;
import core.Coordinate;
import core.MatchSetting;
import core.Move;
import core.Tile;

public class DefaultEngine extends Engine {

	private Board gameBoard;

	public DefaultEngine(String host, int port, MatchSetting match, int position)
			throws UnknownHostException, IOException {
		super(new Socket(host, port), match, position);
		gameBoard = new Board(11,6);
	}

	public void run() {
		System.out.println("default engine started running");
		InputStreamReader ISR = new InputStreamReader(System.in);
		BufferedReader BR = new BufferedReader(ISR);
		int counter = 0;

		while (true) {
			try {
				String[] msg = messageHandler.receiveMessage();
				

				if (msg[0].equals("id")) {
					messageHandler.reply("" + this.playerId);
				} else if (msg[0].equals("name")) {
					messageHandler.reply("Default_Engine");
				} else if (msg[0].equals("genmove")) {
					for (int i = 1; i < msg.length; i++) {
						System.out.println(msg[i]);
					}
					System.out.println("WHAT IS YOUR REPLY?");
					String[] reply = messageHandler.splitMessage(BR.readLine());

					Tile tile = new Tile(Integer.parseInt(reply[1]),
							Integer.parseInt(reply[2]));
					tile.setRotation(Integer.parseInt(reply[3]), 6);
					Coordinate coord = new Coordinate(
							Integer.parseInt(reply[4]),
							Integer.parseInt(reply[5]));
					Move move = new Move(tile, coord);
					gameBoard.makeMove(move);
					System.out.println(gameBoard);
					messageHandler.reply(reply[1], reply[2], reply[3],
							reply[4], reply[5]);
				} else if (msg[0].equals("playmove")) {
					System.out.println("playmove received");
					String[] moveReply = msg;
					
					Tile tile = new Tile(Integer.parseInt(moveReply[1]),
							Integer.parseInt(moveReply[2]));
					tile.setRotation(Integer.parseInt(moveReply[3]), 6);
					Coordinate coords = new Coordinate(
							Integer.parseInt(moveReply[4]),
							Integer.parseInt(moveReply[5]));
					Move move = new Move(tile, coords);
					gameBoard.makeMove(move);
					System.out.println(gameBoard);

				} else {
					System.out.println("WHAT IS YOUR REPLY?");
					messageHandler.replyBrute(BR.readLine());
				}

			} catch (Exception e) {
				messageHandler.errorReply("" + e.toString());
			}
		}
	}
}
