package engines;

import java.util.ArrayList;
import java.util.Collection;

import core.Board;
import core.BoardInterface;
import core.Rack;
import core.Tile;

public abstract class Evaluator {

	public double evaluate(ScoreKeeper scores,BoardInterface gameBoard, ArrayList<Rack> racks ,int playerId){
		double eval = 0;
		int[] score = scores.getScore(playerId).getScore();
		int[] moveScore = scores.calculateScore(gameBoard);
		
		
		for(int i = 0; i< score.length;i++){
			eval += moveScore[i]/(double)score[i];
		}
		return eval;
	}



}
