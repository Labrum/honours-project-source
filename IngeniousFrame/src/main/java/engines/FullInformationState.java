package engines;

import java.util.ArrayList;

import minimax.SearchState;
import core.BoardInterface;
import core.Rack;
import core.Tile;

public class FullInformationState extends SearchState {

	private ArrayList<Tile> searchBag;
	private int bagIndex =0;
	/*
	 * Alpha and beta values can be ignored for normal minimax
	 */
	double alpha = Double.MIN_VALUE +1;
	double beta = Double.MAX_VALUE;
	public FullInformationState(BoardInterface board, ScoreKeeper scores,
			ArrayList<Rack> racks,ArrayList<Tile> bag,int depth) {
		super(board, scores, racks,depth);
		setSearchBag(new ArrayList<Tile>());
		for(Rack r : racks){
			setBagIndex(getBagIndex() + r.size());
		}
		for(Tile t : bag){
			getSearchBag().add(t.copy());
		}
		
	}
	public ArrayList<Tile> getSearchBag() {
		return searchBag;
	}
	public void setSearchBag(ArrayList<Tile> searchBag) {
		this.searchBag = searchBag;
	}
	public int getBagIndex() {
		return bagIndex;
	}
	public void setBagIndex(int bagIndex) {
		this.bagIndex = bagIndex;
	}

}
