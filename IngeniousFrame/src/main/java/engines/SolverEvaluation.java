package engines;

import java.util.ArrayList;

import core.BoardInterface;
import core.Rack;

public class SolverEvaluation extends Evaluator {
	@Override
	public double evaluate(ScoreKeeper scores,BoardInterface board, ArrayList<Rack> racks,int playerId){
		if(board.full()){
			return scores.getScore(playerId).compare(scores.getScore(playerId), scores.getScore(1-playerId));
		}else{
			return 0;
		}
	}
}
