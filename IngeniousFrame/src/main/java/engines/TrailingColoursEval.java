package engines;

import java.util.ArrayList;
import java.util.Arrays;

import core.Board;
import core.BoardInterface;
import core.Rack;
import core.Tile;

public class TrailingColoursEval extends Evaluator {
	public TrailingColoursEval(){
		
	}
	@Override
	public double evaluate(ScoreKeeper scores,BoardInterface board, ArrayList<Rack> racks,int playerId){
		int playerScore[] = scores.getScore(playerId).getScore();
		int[] newScore = scores.calculateScore(board);
		
		for(int i = 0; i<playerScore.length;i++){
			playerScore[i] += newScore[i];
			
		}
		double ret=0;
		boolean[] colours = new boolean[playerScore.length];
		for(int i = 0; i<scores.getNumPlayers(); i++){
			if(i!=playerId){
				int opponentScore[] = scores.getScore(i).getScore();
				for(int j = 0; j < playerScore.length; j++){
					if(playerScore[j]< opponentScore[j]){
						colours[j]=true;
					}
				}
			}
		}
		for(int i = 0; i < playerScore.length; i++){
			if(colours[i]){
				ret--;
			}else{
				ret++;
			}
		}
		int sumpoints = 0;
		for(int i : newScore){
			sumpoints += i;
		}
		
		sumpoints = sumpoints/(18*board.getNumColours());
		
		return ret+sumpoints;
	}
	
}
