package engines;
import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;

import core.BoardInterface;
import core.Coordinate;
import core.MatchSetting;
import core.Move;
import core.Rack;
import core.Tile;
import protocol.Message;
import protocol.MessageHandler;


public abstract class Engine implements Runnable {

	public BoardInterface gameBoard;
	public IMoveController moveController;
	public int playerId;
	public ArrayList<Tile> rack = new ArrayList<Tile>();
	public ArrayList<Rack> racks = new ArrayList<Rack>();
	public MessageHandler messageHandler;
	public ScoreKeeper scoreKeeper;
	public ArrayList<Tile> expectedBag = new ArrayList<Tile>();
	public MatchSetting matchSetting;
	public Evaluator evaluator = new TrailingColoursEval();
	
	public Engine(MatchSetting match,int playerId){		
		for(int i = 0; i< match.getNumPlayers();i++){
			racks.add(new Rack(match.getVariants()[i].getRackSize()));
		}
		this.playerId = playerId;
		this.matchSetting = match;
		this.scoreKeeper = new ScoreKeeper(match.getNumPlayers(),match.getNumColours());
	}
	
	public Engine(Socket socket,MatchSetting match, int playerId){
		messageHandler = new MessageHandler(socket);

		for(int i = 0; i< match.getNumPlayers();i++){
			racks.add(new Rack(match.getVariants()[i].getRackSize()));
		}
		this.playerId = playerId;
		this.matchSetting = match;
		this.scoreKeeper = new ScoreKeeper(match.getNumPlayers(),match.getNumColours()); 
	}
	
	
	public String engineName(){
		return "Default Engine Name";
	}
	
	public void receiveBag(String [] msg){
		Tile next;
		for (int i = 1; i < msg.length; i = i + 2) {
			next = new Tile(Integer.parseInt(msg[i]),
					Integer.parseInt(msg[i + 1]));
			expectedBag.add(next);
			System.out.println("TILE ADDED TO BAG : " + next);
		}
	}
	public void receiveRack(String [] msg){
		Tile next;
		int id = Integer.parseInt(msg[1]);
		for (int i = 2; i < msg.length; i = i + 2) {
			next = new Tile(Integer.parseInt(msg[i]),
					Integer.parseInt(msg[i + 1]));
			racks.get(id).add(next);
			System.out.println("TILE ADDED TO RACK : " + next);
		}
	}
	
	public void receiveDraw(String [] msg){
		int id = Integer.parseInt(msg[1]);
		Tile tile = new Tile(Integer.parseInt(msg[2]),
				Integer.parseInt(msg[3]));
		racks.get(id).add(tile);
	}
	
	public void receivePlayMove(String [] msg){
		System.out.println("playmove received");
		String[] moveReply = msg;
		System.out.println(moveReply.length);

		Tile tile = new Tile(Integer.parseInt(moveReply[1]),
				Integer.parseInt(moveReply[2]));
		tile.setRotation(Integer.parseInt(moveReply[3]), 6);
		Coordinate coords = new Coordinate(
				Integer.parseInt(moveReply[4]),
				Integer.parseInt(moveReply[5]));
		Move move = new Move(tile, coords);
		gameBoard.makeMove(move);
		System.out.println(gameBoard);
	}
	
	public void receiveGenerateMove(String [] msg){
		Move move = moveController.generateMove(this,evaluator);
		messageHandler.reply(move);
		gameBoard.makeMove(move);
		racks.get(playerId).remove(move.getTile());
	}
	public void run() {

		while (true) {
			try {
				String[] msg = messageHandler.receiveMessage();

				if (msg[0].equals("id")) {
					messageHandler.reply("" + this.playerId);
				} else if (msg[0].equals("name")) {
					messageHandler.reply(engineName());
				} else if (msg[0].equals("setbag")){
					receiveBag(msg);
				} else if (msg[0].equals("setrack")) {
					receiveRack(msg);
				} else if (msg[0].equals("genmove")) {
					System.out.println("GENMOVE MESSAGE RECEIVED FOR :"+this.playerId);
					receiveGenerateMove(msg);
				} else if (msg[0].equals("playmove")) {
					receivePlayMove(msg);
				} else if (msg[0].equals("draw")) {
					receiveDraw(msg);
				} else {
					System.out.println("UNEXPECTED TRANSMISSION");
				}

			} catch (Exception e) {
				break;
			}
		}
	}
}
