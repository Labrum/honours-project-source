package mcts;

public class UCTUpdater implements TreeUpdater {

	public void backupValue(SimpleNode node, int result) {
		SearchNode iterator = node;
		while (iterator != null) {
			iterator.addPlayout((result + 1) / (double) 2);
			result = -result;
			iterator = iterator.getParent();
		}
		
	}
}
