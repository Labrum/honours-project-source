package mcts;

import core.Move;

public interface TreeDescender {
	public Move bestPlayMove(SearchNode root);
	public SearchNode bestSearchMove(SearchNode root);
}