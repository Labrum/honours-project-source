package mcts;

import java.util.ArrayList;

import core.Board;
import core.Move;
import core.Rack;

/**
 * 
 * MCTS SearchNode interface
 * 
 * @author steven
 *
 */
public interface SearchNode {

	public ArrayList<SearchNode> getChildren();

	public Rack getRack();
	
	public Move getMove();

	public Board getBoard();

	public long getZobristHash();

	public int getVisits();

	public double getWins();

	public void setVisits(int v);

	public void setWins(int w);

	public boolean expandable();

	public SearchNode expandAChild();

	public SearchNode getParent();

	public void addPlayout(double d);

	public short getCurrentPlayer();
	
	public void transpositionUpdate(SearchNode node);
	
}
