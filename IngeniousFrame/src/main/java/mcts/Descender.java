package mcts;

import java.util.ArrayList;

import core.Move;


public class Descender implements TreeDescender {
	
	public Move bestPlayMove(SearchNode root) {

		int highestVisits = -1;
		SearchNode bestNode = null;
		ArrayList<SearchNode> children = root.getChildren();

		for (SearchNode node : children) {
			if (node.getVisits() > highestVisits) {
				highestVisits = node.getVisits();
				bestNode = node;
			}
		}
		return bestNode.getMove();
	}

	public SearchNode bestSearchMove(SearchNode root) {
		SearchNode bestNode = root;

		while(!bestNode.expandable()){
			bestNode = bestChild(bestNode);
			
		}				
		return bestNode;
	}

	public SearchNode bestChild(SearchNode node){
		ArrayList<SearchNode> children = node.getChildren();

		double highestValue = this.searchValue(children.get(0));
		SearchNode bestMove = children.get(0);
		for(SearchNode child : children){
			double childValue =this.searchValue(child);
			if( childValue > highestValue){
				bestMove = child;
				highestValue = childValue; 
			}
		}
		return bestMove;		
	}
	
	public double searchValue(SearchNode node) {
		
		return node.getWins()/node.getVisits();
	}

}
