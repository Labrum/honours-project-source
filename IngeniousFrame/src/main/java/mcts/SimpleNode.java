package mcts;

import java.util.ArrayList;

import core.BagSequence;
import core.Board;
import core.Move;
import core.Rack;
import core.Tile;

public class SimpleNode implements SearchNode {
	public int numberOfVisits;
	public double numberOfWins;
	private SimpleNode parent = null;
	protected ArrayList<SearchNode> expandedChildren = new ArrayList<SearchNode>();
	protected ArrayList<Move> unExpandedMoves = new ArrayList<Move>();
	protected final Board gameBoard;
	protected short nextPlayer;
	private short currentPlayer;
	private Move originMove;
	protected ArrayList<Rack> rack;
	protected ArrayList<Tile> bag;
	private int depth;
	private int bagSkip = 0;

	public SimpleNode(Board board, Move move, short player,
			ArrayList<Rack> rack, ArrayList<Tile> bag, SimpleNode parent) {
		this.parent = parent;
		for (Rack r : rack) {
			bagSkip += r.size();
		}
		if (parent != null) {
			depth = this.parent.depth + 1;
		} else {
			depth = 0;
		}
		this.rack = rack;
		this.bag = bag;
		this.gameBoard = board.copy();

		this.originMove = move;
		numberOfVisits = 0;
		numberOfWins = 0;
		currentPlayer = player;
		if (player == 1) {
			nextPlayer = 2;
		} else {
			nextPlayer = 1;
		}
		if (originMove != null) {
			gameBoard.makeMove(originMove);
		}
		unExpandedMoves = this.getMoves();
	}

	private ArrayList<Move> getMoves() {
		if (this.originMove != null) {
			gameBoard.makeMove(this.originMove);
			ArrayList<Move> moves = gameBoard.generateMoves(rack
					.get(currentPlayer-1));
			gameBoard.undoMove();
			return moves;
		} else {
			return gameBoard.generateMoves(rack.get(currentPlayer-1));
		}

	}

	public ArrayList<SearchNode> getChildren() {
		return expandedChildren;
	}

	public long getZobristHash() {
		// TODO Auto-generated method stub
		return gameBoard.hashCode();
	}

	public void setVisits(int visits) {
		numberOfVisits = visits;
	}

	public int getVisits() {
		// TODO Auto-generated method stub
		return numberOfVisits;
	}

	public void setWins(int wins) {
		numberOfWins = wins;
	}

	public double getWins() {
		// TODO Auto-generated method stub
		return numberOfWins;
	}

	public boolean expandable() {

		return unExpandedMoves.size() > 0 && (!gameBoard.full());
	}

	public Rack getRack() {
		// TODO Auto-generated method stub
		return this.rack.get(currentPlayer);
	}

	public Move getMove() {
		// TODO Auto-generated method stub
		return this.originMove;
	}

	public Board getBoard() {
		// TODO Auto-generated method stub
		return this.gameBoard.copy();
	}

	public SearchNode expandAChild() {
		int index = (int) (Math.random() * unExpandedMoves.size());
		Move randomChildMove = unExpandedMoves.remove(index);
		rack.get(currentPlayer - 1).remove(randomChildMove.getTile());
		if (bagSkip + depth -1 < bag.size()) {
			rack.get(currentPlayer - 1).add(bag.get(bagSkip + depth-1));
		}else{
			int top = (int) (Math.random() * this.gameBoard.getNumColours());
			int bottom = (int) (Math.random() * this.gameBoard.getNumColours());
			rack.get(currentPlayer-1).add(new Tile(top,bottom));
		}
		SimpleNode expandedNode = new SimpleNode(gameBoard, randomChildMove,
				nextPlayer, rack, bag, this);

		expandedChildren.add(expandedNode);
		return expandedNode;
	}

	public SearchNode getParent() {
		// TODO Auto-generated method stub
		return this.parent;
	}

	public void addPlayout(double d) {
		this.numberOfWins = this.numberOfWins + d;

	}

	public short getCurrentPlayer() {
		// TODO Auto-generated method stub
		return this.currentPlayer;
	}

	public void transpositionUpdate(SearchNode node) {
		// TODO Auto-generated method stub

	}

}
