package mcts;

import java.util.ArrayList;

import core.Board;
import core.Move;
import core.Rack;
import core.Tile;

public class DefaultPolicy {

	public static int playouts = 0;
	public static long timeSpentPlaying = 0;
	
	public static int playout(SearchNode node,short player){
		SimpleNode sNode = (SimpleNode) node;
		
		long startTime = System.currentTimeMillis();
		short originalPlayer = player;

		ArrayList<Move> moves = sNode.gameBoard.generateMoves(sNode.rack.get(0));
		int numberOfMoves = 0;
		while(moves.size()>0 && !sNode.gameBoard.full()){
			int index = (int)(Math.random()*moves.size());
			while(!sNode.gameBoard.validMove(moves.get(index))){
				index = (int)(Math.random()*moves.size());
			}
			sNode.gameBoard.makeMove(moves.get(index));
			if(player == 1){
				player = 2;
			}else{
				player =1;
			}
			moves.remove(index);
			numberOfMoves++;
		}
	//	System.out.println("NUMBER_OF_MOVES MADE IN PLAYOUT : "+numberOfMoves);
	//	System.out.println("NUMBER_OF_MOVES MADE ON board : "+sNode.gameBoard.getMoveHistory().size());
		
		for(int i = 0; i<numberOfMoves;i++){
			sNode.gameBoard.undoMove();
		}
		playouts++;
		int result = 0;
		long endTime = System.currentTimeMillis();
		timeSpentPlaying += (endTime -startTime);
		return result;
	}
	
}
