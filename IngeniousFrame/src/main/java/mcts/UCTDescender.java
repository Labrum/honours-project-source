package mcts;

import static java.lang.Math.sqrt;

public class UCTDescender extends Descender {
	
	@Override
	public double searchValue(SearchNode node) {
		double parentPlayouts=0;

		parentPlayouts = node.getParent().getVisits();	
		
		double barX = node.getWins()/node.getVisits();
		double C = 1/sqrt(2);
		double uncertainty  = 2*C*sqrt(2*Math.log(parentPlayouts)/node.getVisits());
		
		return barX+uncertainty;
	}
}
