package mcts;

import core.Board;
import core.Constants;
import core.Move;
import engines.Engine;
import engines.Evaluator;
import engines.IMoveController;

public class MCTS implements IMoveController {

	Board gameBoard;
	
	TreeUpdater updater;
	TreeDescender descender;
	SimpleNode root;
	
	public Move generateMove(Engine engine, Evaluator evaluator) {
		gameBoard = (Board)engine.gameBoard;
		long timeInit = System.currentTimeMillis();
		long endTime = timeInit + Constants.TURN_LENGTH;
		descender = new UCTDescender();
		updater = new UCTUpdater();

		root = new SimpleNode(gameBoard, null,(short) engine.playerId,engine.racks, engine.expectedBag, null);
		int numberOfPlayouts = 0;
		while(System.currentTimeMillis() < endTime){
			SimpleNode bestNode  = (SimpleNode) descender.bestSearchMove(root);
			SimpleNode newNode = (SimpleNode) bestNode.expandAChild();
			int result=0;
			numberOfPlayouts++;
			result = DefaultPolicy.playout(newNode, (short) newNode.getCurrentPlayer());
			
			updater.backupValue(newNode, result);
		}		
		System.out.println("NUMBER OF PLAYOUTS : "+numberOfPlayouts);
		return descender.bestPlayMove(root);
	}

}
