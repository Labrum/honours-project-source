package tests;

import static org.junit.Assert.*;
import core.Coordinate;

import org.junit.Test;



public class CoordinateTest {

	public static final int x = 1;
	public static final int y = 3;
	
	
	Coordinate coord1 = new Coordinate(x,y);
	Coordinate coord2 = new Coordinate(-x,-y);
	
	@Test
	public void equals(){
		Coordinate coordTwo = new Coordinate(-x,-y);
		assertEquals(true,coordTwo.equals(coord2));
	}
	@Test
	public void unequals(){
		Coordinate coordTwo = new Coordinate(x,-y);
		assertEquals(false,coordTwo.equals(coord2));
	}
	@Test
	public void unequals1(){
		Coordinate coordTwo = new Coordinate(-x,y);
		assertEquals(false,coordTwo.equals(coord2));
	}
	@Test
	public void unequals2(){
		Object coordTwo= new Integer(4);
		assertEquals(false,coord2.equals(coordTwo));
	}
	
	@Test
	public void intSum() {
		Coordinate sumCoord = coord1.add(coord2);
		assertEquals(sumCoord,new Coordinate(0,0));
	}
	
	@Test
	public void intSub() {
		Coordinate sumCoord = coord1.sub(coord2);
		assertEquals(sumCoord,new Coordinate(2*x,2*y));
	}
	
	@Test
	public void doubleSum(){
		Coordinate dub = new Coordinate(-1*(double)x,-1*(double)y);
		Coordinate sumCoord = coord1.add(dub);
		assertEquals(sumCoord,new Coordinate(0,0));
	}
	
	@Test
	public void doubleNegate(){
		Coordinate dub = new Coordinate((double)x,(double)y).negate();
		Coordinate sumCoord = coord1.add(dub);
		assertEquals(sumCoord,new Coordinate(0,0));
	}
	
	
}
