package tests;

import static org.junit.Assert.*;

import java.util.Stack;

import org.junit.Test;

import core.Board;
import core.Coordinate;
import core.Move;
import core.Tile;

public class BoardTest {
	Board board = new Board(11,6);
	@Test
	public void capacity() {
		System.out.println("The capacity test runs");
		assertEquals(board.capacity(11),91);
	}
	@Test
	public void capacityException() {
	 try{
		Board b = new Board(1,4);
	 }catch(Exception e){
		 assertTrue(true);
	 }
	}
	@Test
	public void capacityVariable() {
		assertEquals(board.CAPACITY,91);
	}
	
	@Test
	public void moveHist() {
		Move move = new Move(new Tile(0,0),new Coordinate(0,0));
		board.makeMove(move);
		Stack<Move> moves = board.getMoveHistory();
		assertEquals(move,moves.pop());
	}
	
}

